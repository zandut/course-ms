/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     1/15/2015 4:54:10 PM                         */
/*==============================================================*/


drop table if exists AMBIL_MATKUL;

drop table if exists CHAPTER;

drop table if exists DOSEN;

drop table if exists HARI;

drop table if exists JADWAL;

drop table if exists JENIS_UJIAN;

drop table if exists KUIS;

drop table if exists MAHASISWA;

drop table if exists MATA_KULIAH;

drop table if exists SEMESTER;

drop table if exists TAHUN_AKADEMIK;

drop table if exists TAHUN_SEMESTER;

drop table if exists TUGAS;

drop table if exists UJIAN;

drop table if exists UJIAN_MATKUL;

/*==============================================================*/
/* Table: AMBIL_MATKUL                                          */
/*==============================================================*/
create table AMBIL_MATKUL
(
   ID_AMBIL             int not null auto_increment,
   NIM                  varchar(9),
   KODE_MATKUL          varchar(12),
   primary key (ID_AMBIL)
);

/*==============================================================*/
/* Table: CHAPTER                                               */
/*==============================================================*/
create table CHAPTER
(
   ID_CHAPTER           int not null auto_increment,
   KODE_MATKUL          varchar(12),
   NAMA_CHAPTER         text,
   CATATAN              mediumtext,
   primary key (ID_CHAPTER)
);

/*==============================================================*/
/* Table: DOSEN                                                 */
/*==============================================================*/
create table DOSEN
(
   KODE_DOSEN           varchar(12) not null,
   NAMA_DOSEN           text,
   EMAIL                text,
   NO_HP                text,
   primary key (KODE_DOSEN)
);

/*==============================================================*/
/* Table: HARI                                                  */
/*==============================================================*/
create table HARI
(
   ID_HARI              int not null auto_increment,
   NAMA_HARI            text,
   primary key (ID_HARI)
);

/*==============================================================*/
/* Table: JADWAL                                                */
/*==============================================================*/
create table JADWAL
(
   ID_HARI              int,
   ID_AMBIL             int,
   JAM                  time,
   RUANGAN              text
);

/*==============================================================*/
/* Table: JENIS_UJIAN                                           */
/*==============================================================*/
create table JENIS_UJIAN
(
   ID_JENIS             int not null auto_increment,
   NAMA_JENIS           text,
   primary key (ID_JENIS)
);

/*==============================================================*/
/* Table: KUIS                                                  */
/*==============================================================*/
create table KUIS
(
   ID_KUIS              int not null auto_increment,
   ID_CHAPTER           int,
   NAMA_KUIS            text,
   TGL                  date,
   KETERANGAN           text,
   NILAI                double,
   primary key (ID_KUIS)
);

/*==============================================================*/
/* Table: MAHASISWA                                             */
/*==============================================================*/
create table MAHASISWA
(
   NIM                  varchar(9) not null,
   NAMA                 text,
   ALAMAT               text,
   PASSWORD             text,
   KODE_AKTIFASI        text,
   primary key (NIM)
);

/*==============================================================*/
/* Table: MATA_KULIAH                                           */
/*==============================================================*/
create table MATA_KULIAH
(
   KODE_MATKUL          varchar(12) not null,
   KODE_DOSEN           varchar(12),
   ID_TAHUN_SEMESTER    int,
   NAMA_MATKUL          text,
   primary key (KODE_MATKUL)
);

/*==============================================================*/
/* Table: SEMESTER                                              */
/*==============================================================*/
create table SEMESTER
(
   ID_SEMESTER          int not null auto_increment,
   NAMA_SEMESTER        text,
   primary key (ID_SEMESTER)
);

/*==============================================================*/
/* Table: TAHUN_AKADEMIK                                        */
/*==============================================================*/
create table TAHUN_AKADEMIK
(
   TAHUN                varchar(10) not null,
   primary key (TAHUN)
);

/*==============================================================*/
/* Table: TAHUN_SEMESTER                                        */
/*==============================================================*/
create table TAHUN_SEMESTER
(
   ID_TAHUN_SEMESTER    int not null auto_increment,
   TAHUN                varchar(10),
   ID_SEMESTER          int,
   IS_AKTIF             int,
   primary key (ID_TAHUN_SEMESTER)
);

/*==============================================================*/
/* Table: TUGAS                                                 */
/*==============================================================*/
create table TUGAS
(
   ID_TUGAS             int not null auto_increment,
   ID_CHAPTER           int,
   JUDUL_TUGAS          text,
   TGL_KUMPUL           date,
   JAM_KUMPUL           time,
   NILAI                double,
   STATUS               int,
   primary key (ID_TUGAS)
);

/*==============================================================*/
/* Table: UJIAN                                                 */
/*==============================================================*/
create table UJIAN
(
   ID_UJIAN             int not null auto_increment,
   ID_JENIS             int,
   NAMA_UJIAN           text,
   TGL                  date,
   KETERANGAN           text,
   primary key (ID_UJIAN)
);

/*==============================================================*/
/* Table: UJIAN_MATKUL                                          */
/*==============================================================*/
create table UJIAN_MATKUL
(
   ID_AMBIL             int,
   ID_UJIAN             int,
   NILAI                double
);

alter table AMBIL_MATKUL add constraint FK_REFERENCE_1 foreign key (NIM)
      references MAHASISWA (NIM) on delete cascade on update cascade;

alter table AMBIL_MATKUL add constraint FK_REFERENCE_2 foreign key (KODE_MATKUL)
      references MATA_KULIAH (KODE_MATKUL) on delete cascade on update cascade;

alter table CHAPTER add constraint FK_REFERENCE_16 foreign key (KODE_MATKUL)
      references MATA_KULIAH (KODE_MATKUL) on delete cascade on update cascade;

alter table JADWAL add constraint FK_REFERENCE_5 foreign key (ID_HARI)
      references HARI (ID_HARI) on delete cascade on update cascade;

alter table JADWAL add constraint FK_REFERENCE_6 foreign key (ID_AMBIL)
      references AMBIL_MATKUL (ID_AMBIL) on delete cascade on update cascade;

alter table KUIS add constraint FK_REFERENCE_18 foreign key (ID_CHAPTER)
      references CHAPTER (ID_CHAPTER) on delete cascade on update cascade;

alter table MATA_KULIAH add constraint FK_REFERENCE_15 foreign key (ID_TAHUN_SEMESTER)
      references TAHUN_SEMESTER (ID_TAHUN_SEMESTER) on delete cascade on update cascade;

alter table MATA_KULIAH add constraint FK_REFERENCE_3 foreign key (KODE_DOSEN)
      references DOSEN (KODE_DOSEN) on delete set null on update set null;

alter table TAHUN_SEMESTER add constraint FK_REFERENCE_13 foreign key (TAHUN)
      references TAHUN_AKADEMIK (TAHUN) on delete cascade on update cascade;

alter table TAHUN_SEMESTER add constraint FK_REFERENCE_14 foreign key (ID_SEMESTER)
      references SEMESTER (ID_SEMESTER) on delete cascade on update cascade;

alter table TUGAS add constraint FK_REFERENCE_17 foreign key (ID_CHAPTER)
      references CHAPTER (ID_CHAPTER) on delete cascade on update cascade;

alter table UJIAN add constraint FK_REFERENCE_12 foreign key (ID_JENIS)
      references JENIS_UJIAN (ID_JENIS) on delete cascade on update cascade;

alter table UJIAN_MATKUL add constraint FK_REFERENCE_11 foreign key (ID_UJIAN)
      references UJIAN (ID_UJIAN) on delete cascade on update cascade;

alter table UJIAN_MATKUL add constraint FK_REFERENCE_8 foreign key (ID_AMBIL)
      references AMBIL_MATKUL (ID_AMBIL) on delete cascade on update cascade;

