(function(c){var b=function(d,e){this.options=e;this.$elementFilestyle=[];this.$element=c(d)};b.prototype={clear:function(){this.$element.val("");this.$elementFilestyle.find(":text").val("")},destroy:function(){this.$element.removeAttr("style").removeData("filestyle").val("");this.$elementFilestyle.remove()},disabled:function(d){if(d===true){if(!this.options.disabled){this.$element.attr("disabled","true");this.$elementFilestyle.find("label").attr("disabled","true");this.options.disabled=true}}else{if(d===false){if(this.options.disabled){this.$element.removeAttr("disabled");this.$elementFilestyle.find("label").removeAttr("disabled");this.options.disabled=false}}else{return this.options.disabled}}},buttonBefore:function(d){if(d===true){if(!this.options.buttonBefore){this.options.buttonBefore=true;if(this.options.input){this.$elementFilestyle.remove();this.constructor();this.pushNameFiles()}}}else{if(d===false){if(this.options.buttonBefore){this.options.buttonBefore=false;if(this.options.input){this.$elementFilestyle.remove();this.constructor();this.pushNameFiles()}}}else{return this.options.buttonBefore}}},icon:function(d){if(d===true){if(!this.options.icon){this.options.icon=true;this.$elementFilestyle.find("label").prepend(this.htmlIcon())}}else{if(d===false){if(this.options.icon){this.options.icon=false;this.$elementFilestyle.find(".glyphicon").remove()}}else{return this.options.icon}}},input:function(d){if(d===true){if(!this.options.input){this.options.input=true;if(this.options.buttonBefore){this.$elementFilestyle.append(this.htmlInput())}else{this.$elementFilestyle.prepend(this.htmlInput())}this.$elementFilestyle.find(".badge").remove();var e="",g=[];if(this.$element[0].files===undefined){g[0]={name:this.$element[0].value}}else{g=this.$element[0].files}for(var f=0;f<g.length;f++){e+=g[f].name.split("\\").pop()+", "}if(e!==""){this.$elementFilestyle.find(":text").val(e.replace(/\, $/g,""))}this.$elementFilestyle.find(".group-span-filestyle").addClass("input-group-btn")}}else{if(d===false){if(this.options.input){this.options.input=false;this.$elementFilestyle.find(":text").remove();var g=[];if(this.$element[0].files===undefined){g[0]={name:this.$element[0].value}}else{g=this.$element[0].files}if(g.length>0){this.$elementFilestyle.find("label").append(' <span class="badge">'+g.length+"</span>")}this.$elementFilestyle.find(".group-span-filestyle").removeClass("input-group-btn")}}else{return this.options.input}}},size:function(d){if(d!==undefined){var f=this.$elementFilestyle.find("label"),e=this.$elementFilestyle.find("input");f.removeClass("btn-lg btn-sm");e.removeClass("input-lg input-sm");if(d!="nr"){f.addClass("btn-"+d);e.addClass("input-"+d)}}else{return this.options.size}},buttonText:function(d){if(d!==undefined){this.options.buttonText=d;this.$elementFilestyle.find("label span").html(this.options.buttonText)}else{return this.options.buttonText}},buttonName:function(d){if(d!==undefined){this.options.buttonName=d;this.$elementFilestyle.find("label").attr({"class":"btn "+this.options.buttonName})}else{return this.options.buttonName}},iconName:function(d){if(d!==undefined){this.$elementFilestyle.find(".glyphicon").attr({"class":".glyphicon "+this.options.iconName})}else{return this.options.iconName}},htmlIcon:function(){if(this.options.icon){return'<span class="glyphicon '+this.options.iconName+'"></span> '}else{return""}},htmlInput:function(){if(this.options.input){return'<input type="text" class="form-control '+(this.options.size=="nr"?"":"input-"+this.options.size)+'" disabled> '}else{return""}},pushNameFiles:function(){var d="",f=[];if(this.$element[0].files===undefined){f[0]={name:this.$element.value}}else{f=this.$element[0].files}for(var e=0;e<f.length;e++){d+=f[e].name.split("\\").pop()+", "}if(d!==""){this.$elementFilestyle.find(":text").val(d.replace(/\, $/g,""))}else{this.$elementFilestyle.find(":text").val("")}},constructor:function(){var i=this,g="",h=this.$element.attr("id"),d=[],j="",f,e;if(h===""||!h){h="filestyle-"+c(".bootstrap-filestyle").length;this.$element.attr({id:h})}j='<span class="group-span-filestyle '+(this.options.input?"input-group-btn":"")+'"><label for="'+h+'" class="btn '+this.options.buttonName+" "+(this.options.size=="nr"?"":"btn-"+this.options.size)+'" '+(this.options.disabled?'disabled="true"':"")+">"+this.htmlIcon()+this.options.buttonText+"</label></span>";g=this.options.buttonBefore?j+this.htmlInput():this.htmlInput()+j;this.$elementFilestyle=c('<div class="bootstrap-filestyle input-group">'+g+"</div>");f=this.$elementFilestyle.find("label");e=f.parent();e.attr("tabindex","0").keypress(function(k){if(k.keyCode===13||k.charCode===32){f.click()}});this.$element.css({position:"absolute",clip:"rect(0,0,0,0)"}).attr("tabindex","-1").after(this.$elementFilestyle);if(this.options.disabled){this.$element.attr("disabled","true")}this.$element.change(function(){var k="";if(this.files===undefined){d[0]={name:this.value}}else{d=this.files}for(var l=0;l<d.length;l++){k+=d[l].name.split("\\").pop()+", "}if(k!==""){i.$elementFilestyle.find(":text").val(k.replace(/\, $/g,""))}else{i.$elementFilestyle.find(":text").val("")}if(i.options.input==false){if(i.$elementFilestyle.find(".badge").length==0){i.$elementFilestyle.find("label").append(' <span class="badge">'+d.length+"</span>")}else{if(d.length==0){i.$elementFilestyle.find(".badge").remove()}else{i.$elementFilestyle.find(".badge").html(d.length)}}}else{i.$elementFilestyle.find(".badge").remove()}});if(window.navigator.userAgent.search(/firefox/i)>-1){this.$elementFilestyle.find("label").click(function(){i.$element.click();return false})}}};var a=c.fn.filestyle;c.fn.filestyle=function(e,d){var f="",g=this.each(function(){if(c(this).attr("type")==="file"){var j=c(this),h=j.data("filestyle"),i=c.extend({},c.fn.filestyle.defaults,e,typeof e==="object"&&e);if(!h){j.data("filestyle",(h=new b(this,i)));h.constructor()}if(typeof e==="string"){f=h[e](d)}}});if(typeof f!==undefined){return f}else{return g}};c.fn.filestyle.defaults={buttonText:"&nbsp;Choose file",iconName:"glyphicon-folder-open",buttonName:"btn-flat btn-default",size:"nr",input:true,icon:true,buttonBefore:true,disabled:false};c.fn.filestyle.noConflict=function(){c.fn.filestyle=a;return this};c(function(){c(".filestyle").each(function(){var e=c(this),d={input:e.attr("data-input")==="false"?false:true,icon:e.attr("data-icon")==="false"?false:true,buttonBefore:e.attr("data-buttonBefore")==="true"?true:false,disabled:e.attr("data-disabled")==="true"?true:false,size:e.attr("data-size"),buttonText:e.attr("data-buttonText"),buttonName:e.attr("data-buttonName"),iconName:e.attr("data-iconName")};e.filestyle(d)})})})(window.jQuery);
/*
 *
 * Copyright (c) 2006-2014 Sam Collett (http://www.texotela.co.uk)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Version 1.4
 * Demo: http://www.texotela.co.uk/code/jquery/numeric/
 *
 */
(function($) {
/*
 * Allows only valid characters to be entered into input boxes.
 * Note: fixes value when pasting via Ctrl+V, but not when using the mouse to paste
  *      side-effect: Ctrl+A does not work, though you can still use the mouse to select (or double-click to select all)
 *
 * @name     numeric
 * @param    config      { decimal : "." , negative : true }
 * @param    callback     A function that runs if the number is not valid (fires onblur)
 * @author   Sam Collett (http://www.texotela.co.uk)
 * @example  $(".numeric").numeric();
 * @example  $(".numeric").numeric(","); // use , as separator
 * @example  $(".numeric").numeric({ decimal : "," }); // use , as separator
 * @example  $(".numeric").numeric({ negative : false }); // do not allow negative values
 * @example  $(".numeric").numeric(null, callback); // use default values, pass on the 'callback' function
 *
 */
$.fn.numeric = function(config, callback)
{
    if(typeof config === 'boolean')
    {
        config = { decimal: config };
    }
    config = config || {};
    // if config.negative undefined, set to true (default is to allow negative numbers)
    if(typeof config.negative == "undefined") { config.negative = true; }
    // set decimal point
    var decimal = (config.decimal === false) ? "" : config.decimal || ".";
    // allow negatives
    var negative = (config.negative === true) ? true : false;
    // callback function
    callback = (typeof(callback) == "function" ? callback : function() {});
    // set data and methods
    return this.data("numeric.decimal", decimal).data("numeric.negative", negative).data("numeric.callback", callback).keypress($.fn.numeric.keypress).keyup($.fn.numeric.keyup).blur($.fn.numeric.blur);
};

$.fn.numeric.keypress = function(e)
{
    // get decimal character and determine if negatives are allowed
    var decimal = $.data(this, "numeric.decimal");
    var negative = $.data(this, "numeric.negative");
    // get the key that was pressed
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    // allow enter/return key (only when in an input box)
    if(key == 13 && this.nodeName.toLowerCase() == "input")
    {
        return true;
    }
    else if(key == 13)
    {
        return false;
    }
    var allow = false;
    // allow Ctrl+A
    if((e.ctrlKey && key == 97 /* firefox */) || (e.ctrlKey && key == 65) /* opera */) { return true; }
    // allow Ctrl+X (cut)
    if((e.ctrlKey && key == 120 /* firefox */) || (e.ctrlKey && key == 88) /* opera */) { return true; }
    // allow Ctrl+C (copy)
    if((e.ctrlKey && key == 99 /* firefox */) || (e.ctrlKey && key == 67) /* opera */) { return true; }
    // allow Ctrl+Z (undo)
    if((e.ctrlKey && key == 122 /* firefox */) || (e.ctrlKey && key == 90) /* opera */) { return true; }
    // allow or deny Ctrl+V (paste), Shift+Ins
    if((e.ctrlKey && key == 118 /* firefox */) || (e.ctrlKey && key == 86) /* opera */ ||
      (e.shiftKey && key == 45)) { return true; }
    // if a number was not pressed
    if(key < 48 || key > 57)
    {
      var value = $(this).val();
        /* '-' only allowed at start and if negative numbers allowed */
        if($.inArray('-', value.split('')) !== 0 && negative && key == 45 && (value.length === 0 || parseInt($.fn.getSelectionStart(this), 10) === 0)) { return true; }
        /* only one decimal separator allowed */
        if(decimal && key == decimal.charCodeAt(0) && $.inArray(decimal, value.split('')) != -1)
        {
            allow = false;
        }
        // check for other keys that have special purposes
        if(
            key != 8 /* backspace */ &&
            key != 9 /* tab */ &&
            key != 13 /* enter */ &&
            key != 35 /* end */ &&
            key != 36 /* home */ &&
            key != 37 /* left */ &&
            key != 39 /* right */ &&
            key != 46 /* del */
        )
        {
            allow = false;
        }
        else
        {
            // for detecting special keys (listed above)
            // IE does not support 'charCode' and ignores them in keypress anyway
            if(typeof e.charCode != "undefined")
            {
                // special keys have 'keyCode' and 'which' the same (e.g. backspace)
                if(e.keyCode == e.which && e.which !== 0)
                {
                    allow = true;
                    // . and delete share the same code, don't allow . (will be set to true later if it is the decimal point)
                    if(e.which == 46) { allow = false; }
                }
                // or keyCode != 0 and 'charCode'/'which' = 0
                else if(e.keyCode !== 0 && e.charCode === 0 && e.which === 0)
                {
                    allow = true;
                }
            }
        }
        // if key pressed is the decimal and it is not already in the field
        if(decimal && key == decimal.charCodeAt(0))
        {
            if($.inArray(decimal, value.split('')) == -1)
            {
                allow = true;
            }
            else
            {
                allow = false;
            }
        }
    }
    else
    {
        allow = true;
    }
    return allow;
};

$.fn.numeric.keyup = function(e)
{
    var val = $(this).val();
    if(val && val.length > 0)
    {
        // get carat (cursor) position
        var carat = $.fn.getSelectionStart(this);
        var selectionEnd = $.fn.getSelectionEnd(this);
        // get decimal character and determine if negatives are allowed
        var decimal = $.data(this, "numeric.decimal");
        var negative = $.data(this, "numeric.negative");

        // prepend a 0 if necessary
        if(decimal !== "" && decimal !== null)
        {
            // find decimal point
            var dot = $.inArray(decimal, val.split(''));
            // if dot at start, add 0 before
            if(dot === 0)
            {
                this.value = "0" + val;
                carat++;
                        selectionEnd++;
            }
            // if dot at position 1, check if there is a - symbol before it
            if(dot == 1 && val.charAt(0) == "-")
            {
                this.value = "-0" + val.substring(1);
                carat++;
                        selectionEnd++;
            }
            val = this.value;
        }

        // if pasted in, only allow the following characters
        var validChars = [0,1,2,3,4,5,6,7,8,9,'-',decimal];
        // get length of the value (to loop through)
        var length = val.length;
        // loop backwards (to prevent going out of bounds)
        for(var i = length - 1; i >= 0; i--)
        {
            var ch = val.charAt(i);
            // remove '-' if it is in the wrong place
            if(i !== 0 && ch == "-")
            {
                val = val.substring(0, i) + val.substring(i + 1);
            }
            // remove character if it is at the start, a '-' and negatives aren't allowed
            else if(i === 0 && !negative && ch == "-")
            {
                val = val.substring(1);
            }
            var validChar = false;
            // loop through validChars
            for(var j = 0; j < validChars.length; j++)
            {
                // if it is valid, break out the loop
                if(ch == validChars[j])
                {
                    validChar = true;
                    break;
                }
            }
            // if not a valid character, or a space, remove
            if(!validChar || ch == " ")
            {
                val = val.substring(0, i) + val.substring(i + 1);
            }
        }
        // remove extra decimal characters
        var firstDecimal = $.inArray(decimal, val.split(''));
        if(firstDecimal > 0)
        {
            for(var k = length - 1; k > firstDecimal; k--)
            {
                var chch = val.charAt(k);
                // remove decimal character
                if(chch == decimal)
                {
                    val = val.substring(0, k) + val.substring(k + 1);
                }
            }
        }
        // set the value and prevent the cursor moving to the end
        this.value = val;
        $.fn.setSelection(this, [carat, selectionEnd]);
    }
};

$.fn.numeric.blur = function()
{
    var decimal = $.data(this, "numeric.decimal");
    var callback = $.data(this, "numeric.callback");
    var val = this.value;
    if(val !== "")
    {
        var re = new RegExp("^\\d+$|^\\d*" + decimal + "\\d+$");
        if(!re.exec(val))
        {
            callback.apply(this);
        }
    }
};

$.fn.removeNumeric = function()
{
    return this.data("numeric.decimal", null).data("numeric.negative", null).data("numeric.callback", null).unbind("keypress", $.fn.numeric.keypress).unbind("blur", $.fn.numeric.blur);
};

// Based on code from http://javascript.nwbox.com/cursor_position/ (Diego Perini <dperini@nwbox.com>)
$.fn.getSelectionStart = function(o)
{
    if(o.type === "number"){
        return undefined;
    }
    else if (o.createTextRange)
    {
        var r;
        if(typeof document.selection == "undefined") {
            //On IE < 9 && IE >= 11 : "document.selection" is deprecated and you should use "document.getSelection()"
            //https://github.com/SamWM/jQuery-Plugins/issues/62
            r = document.getSelection();
        } else {
            r = document.selection.createRange().duplicate();
            r.moveEnd('character', o.value.length);
        }
        if (r.text == '') return o.value.length;

        return o.value.lastIndexOf(r.text);
    } else {
        try { return o.selectionStart; }
        catch(e) { return 0; }
    }
};

// Based on code from http://javascript.nwbox.com/cursor_position/ (Diego Perini <dperini@nwbox.com>)
$.fn.getSelectionEnd = function(o)
{
    if(o.type === "number"){
        return undefined;
    }
    else if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveStart('character', -o.value.length)
        return r.text.length
    } else return o.selectionEnd
}

// set the selection, o is the object (input), p is the position ([start, end] or just start)
$.fn.setSelection = function(o, p)
{
    // if p is number, start and end are the same
    if(typeof p == "number") { p = [p, p]; }
    // only set if p is an array of length 2
    if(p && p.constructor == Array && p.length == 2)
    {
        if(o.type === "number") {
            o.focus();
        }
        else if (o.createTextRange)
        {
            var r = o.createTextRange();
            r.collapse(true);
            r.moveStart('character', p[0]);
            r.moveEnd('character', p[1]);
            r.select();
        }
        else {
            o.focus();
            try{
                if(o.setSelectionRange)
                {
                    o.setSelectionRange(p[0], p[1]);
                }
            } catch(e) {
            }
        }
    }
};

})(jQuery);