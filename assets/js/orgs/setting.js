jQuery(document).ready(function() {
    var opts = {
            chartElement : '#chart', //your tree container
            dragAndDrop  : true
    };
    $("#chart").html(""); //clean your container
    $("#org").jOrgChart(opts); //creates the jOrgChart

    var opts2 = {
            chartElement : '#charts', //your tree container
            dragAndDrop  : false
    };
    $("#charts").html(""); //clean your container
    $("#orgs").jOrgChart(opts2);

    var regx = /\w*(row)/;
    var add_to_node, del_node, classList;
    var oTable = $("#example1").dataTable({
        "fnDrawCallback": function (oSettings) {
            $("[data-toggle='confirmation']").confirmation();
        },
        "aLengthMenu": [
            [2],
            [2]
        ],
        "iDisplayLength" : 2,
        "bLengthChange": false
    });

    $("#getjson").on('click', function() {
            var hierarchy = [];

            $("#org li").each(function(){
                var uid = $(this).attr("id");
                var name = $(this).attr("id");
                var kode = $(this).attr('data-kode');
                name = name.substring(1, name.length-1);
                var hidSTR = "";
                var x = '';
                var hid = $(this).parents("li");
                if(hid.length == 0) //If this object is the root user, substitute id with "orgName" so the DB knows it's the name of organization and not a user
                {
                    hidSTR = "parent";
                    var user = new Object();
                    user.key = kode;
                    user.hierarchy = hidSTR;
                    user.jabatan = $(this).attr('data-jabatan');
                    hierarchy.push(user);
                }else{
                    // console.log(hid[0].dataset.kode);
                    for(var i=hid.length-1; i>=0; i--)
                    {
                        if(i != hid.length-1)
                        {
                            hidSTR = hid[i].dataset.kode;
                            x = hid[i].dataset.jabatan;
                        }else{
                            hidSTR = hid[i].dataset.kode;
                            x = hid[i].dataset.jabatan;
                        }
                    }
                var user = new Object();
                    user.key = kode;
                    user.hierarchy = hidSTR;
                    user.jabatan = $(this).attr('data-jabatan');
                    hierarchy.push(user);
                }
            });
            json_text = JSON.stringify(hierarchy, null, 2);
            console.log(json_text);
            var url = $("#urls").html();
            var kode = $("#kodes").html();
            if (json_text != '[]'){
                $.ajax({
                  type: "POST",
                  url: url+'organization/settings/store/'+kode,
                  data: {'data': json_text },
                  beforeSend: function() {
                    $(this).html("Menyimpan....");
                  },
                  success: function(data) {
                    if (data == 'ok')
                    {
                        location.reload();
                    }else{
                        $(this).html("Gagal Menyimpan, coba lagi.");
                    }
                  },
                  // dataType: "json",
                  // contentType: 'application/json; charset=UTF-8'
                });
            }
            // alert("Check console")
        });

        $(".del").live("click", function(e){
            var nodo=$(this);

            if(!nodo.parent().parent().hasClass("temp")){
                var nodeDiv = nodo.parent().parent();
                var cu = nodeDiv.find("a").attr("rel");
                classList = nodeDiv.attr('class').split(/\s+/);
                $.each(classList, function(index,item) {
                    if(item != "temp" && item != "node" && item != "child" && item != "ui-draggable" && item != "ui-droppable" && !regx.test(item) ){
                        del_node = item;
                    }
                });
                var element = $("li."+del_node+":not('.temp, #upload-chart li')").removeAttr("class").addClass("node").addClass("child");
                remChild(element);
                var opts = {
                        chartElement : '#chart', //your tree container
                        dragAndDrop  : true
                };
                $("#chart").html(""); //clean your container
                $("#org").jOrgChart(opts);
            }

        });

    function remChild(removing){
        $("#upload-chart").append(removing);
        $("#upload-chart ul li").each(function(){
            var Orgli = $(this).removeAttr("class").addClass("node").addClass("child").clone();
            $(this).remove();
            $("#upload-chart").append(Orgli);
        });
        $("#upload-chart ul").remove();
        var sideLi = $("#upload-chart").html();
        $("#upload-chart").empty();
        $("#upload-chart").append(sideLi);
    }

    $(".adds").live('click', function(){
        var el=$(this);
        var kode    = el.parent().prev().prev().html();
        var nama    = el.parent().prev().html();
        var txt     = '';
        var org     = $("#org");

        if (org.children().length == 0){
            txt = '<li id="s'+Math.random().toString(36).substring(8)+'s" data-jabatan="'+kode+'" data-kode="'+Math.random().toString(36).substring(8)+'" class="uniq1 root nrow"><p class="titles">'+nama+'</p></li>';
            $("#org").append(txt);
        }else{
            txt = '<li id="s'+Math.random().toString(36).substring(8)+'s" data-jabatan="'+kode+'" data-kode="'+Math.random().toString(36).substring(8)+'" class="node child"><p class="titles">'+nama+'</p></li>';
            $("#upload-chart").append(txt);
        }

        var opts = {
                chartElement : '#chart', //your tree container
                dragAndDrop  : true
        };
        $("#chart").html(""); //clean your container
        $("#org").jOrgChart(opts);
    });

    $("#officeSel").on('change', function(){
        var load = $("#links");
        var url = "";
        var current_url = $(location).attr('href');
        var params_name = "kantor"

        var has_month = false;
        var has_params = false

        if(current_url.indexOf(params_name) != -1){
            has_month = true;
        }

        if(current_url.indexOf("?") != -1){
            has_params = true;
        }
        if (has_params){
            if (has_month){
              var regEx = /([?&]kantor)=([^#&]*)/g;
              url = current_url.replace(regEx, '$1='+this.value);
            }
        }else{
            url = current_url+"?kantor="+this.value;
        }

        load.attr('href', url);
        load[0].click();
    });
});