// kompetensi
$(document).ready(function(){
    $('#test').select2({
  			placeholder: "Select report type",

		});
    var $soft = true;
    var $hard = true;
   	$("#generate_soft").click( function()
			{
				$load = $("#holders");
                $jumlah_soft_kompetensi = document.getElementById('jumlah_soft_kompetensi').value;
                if ($jumlah_soft_kompetensi > 0 && $soft){
    				$("#jumlah_soft_kompetensi").prop( "disabled", true );
    				for ($i = 1; $i <= $jumlah_soft_kompetensi; $i++)
    				{
    					$("#classAdd").append($load.html());
    				}
                    $('select').select2();
                    $(".idx").each(function(){
                        var attr = $(this).attr('title');
                        if (attr != ''){
                            $(this).addClass('hide');
                        }
                    });
                    $soft = false;
                }

           }
      );
   	$("#generate_hard").click( function()
			{
				$load = $("#holders");
                $jumlah_hard_kompetensi = document.getElementById('jumlah_hard_kompetensi').value;
                if ($jumlah_hard_kompetensi > 0 && $hard){
    				$("#jumlah_hard_kompetensi").prop( "disabled", true );

    				for ($i = 1; $i <= $jumlah_hard_kompetensi; $i++)
    				{
    					$("#classAdd").append($load.html());
    				}
                    $('select').select2();
                    $(".idx").each(function(){
                        var attr = $(this).attr('title');
                        if (attr != ''){
                            $(this).addClass('hide');
                        }
                    });
                    $hard = false;
                }

           }
      );
});