// kompetensi
$(document).ready(function(){
    var urlx = $("#urlx").html();

    $('#jadwalInput').daterangepicker();
   
    $("#kompetensiSelect").on('change', function(){
        $levels = $(".levels");
        if (this.value == 'bidang'){
            $levels.eq(6).removeClass('hide');
            $levels.eq(7).removeClass('hide');
            $levels.eq(1).addClass('hide');
        }else{
            $levels.eq(6).addClass('hide');
            $levels.eq(7).addClass('hide');
            $levels.eq(1).removeClass('hide');
        }
    });

    $("#selz").on('change', function(){

        if ($(this).val() != 'Dapat Dipertimbangkan dengan Catatan'){
            $("#selz2").addClass('hide');
            $("#selz2").children().val('');
        }else{
            $("#selz2").removeClass('hide');
        }
    });

    $(".example").dataTable({
        "fnDrawCallback": function (oSettings) {
            $("[data-toggle='confirmation']").confirmation();
        },
        "bFilter" : false,
        "aLengthMenu": [
            [8],
            [8]
        ],
        "iDisplayLength" : 8,
        "bLengthChange": false
    });

    $("#kompetensiSelect2").on('change', function(){
        $load = $("#holders");
        $load2 = $("#holders2");
        $isi = $("#isi");
        if (this.value != ''){
            $.ajax({
              url: urlx+"master/fitnproper/ajax/"+this.value,
              beforeSend: function() {
                $isi.html($load.html());
              },
              success: function(data) {
                $isi.html(data);
                $("select").select2();
              },
              error: function(data) {
                $isi.html($load2.html());
              }

            })
        }
    });

    $("#nipegSelect").on('change', function(){
        $load = $("#holders");
        $load2 = $("#holders2");
        $isi = $("#isi");
        if (this.value != ''){
            $.ajax({
              url: urlx+"master/fitnproper/ajax/"+this.value,
              beforeSend: function() {
                $isi.html($load.html());
              },
              success: function(data) {
                $isi.html(data);
                $("select").select2();
              },
              error: function(data) {
                $isi.html($load2.html());
              }

            })
        }
    });

    $("#emSelt").on('change', function(){
          $.ajax({
            dataType: "json",
            url: urlx+"master/evaluation/ajax/"+this.value,
            success: function(data) {
              // console.log(data);
              $("#hard").val(data.nilai_assesment_hard_competency);
              $("#soft").val(data.nilai_assesment_soft_competency);
            }
          })
    });

    $("#selJabs").on('change', function(){
        var urlxz = $("#urlxz");
        var nipeg = $("#nipeg").html();
        urlxz.attr('href', urlx+"evaluations/proyeksi_hard/"+nipeg+"/"+$(this).val());
        urlxz[0].click();
    });

    //jadwal
    $(".updateJadwal").on('click', function(){
        $classes = $(this).attr('class');
        $idx = $classes.split(' ')[1];
        $('.btnx').html('Update');
        document.getElementById("idx").value = ($idx);
        document.getElementById("jadwalInput").value = ($(".toupdate-"+$idx).html());
        return false;
    });

    $(".btnxreset").on('click', function(){
        document.getElementById("idx").value = ('');
        $('.btnx').html('Submit');
    });

    //files
    $(":file").filestyle();

    // fit n proper
    $("#selPeng").on('change', function(){
        $('.peng').addClass('hide');
        for (var i = 1; i <= this.value; i++) {
            $(".peng-"+i).removeClass('hide');
        };
    });

    $("#selJabatan").on('change', function(){
        $load = $("#holders");
        $load2 = $("#holders2");
        $isi = $("#isi");
        $jabatan = this.value;
        $kantor  =  document.getElementById("selKantor").value;
        $jabatan = $jabatan == '' ? 'null' : $jabatan;
        $kantor = $kantor == '' ? 'null' : $kantor;
        $lbl = $('#labl').html();

        if (this.value != ''){
            $.ajax({
              url: urlx+"fitnproper/master/ajax/"+$jabatan+"/"+$kantor+"/"+$lbl,
              beforeSend: function() {
                $isi.html($load.html());
              },
              success: function(data) {
                $isi.html(data);
                $("select").select2();
              },
              error: function(data) {
                $isi.html($load2.html());
              }

            })
        }
    });

    $("#empSelect").on('change', function(){
        var load = $(".placeholds");
        var val = $(this).val();
        var isi = $("#softs");
        // if (this.value != ''){
            $.ajax({
              url: urlx+"master/assessment/ajax/"+val,
              beforeSend: function() {
                isi.html('');
                load.show();
              },
              success: function(data) {
                isi.html(data);
                load.hide();
                $("select").select2();
              },
              error: function(data) {
                load.show();
                load.html('gagal mengambil data, coba lagi..');
              }
            })
        // }
    });

    $("#selKantor").on('change', function(){
        $load = $("#holders");
        $load2 = $("#holders2");
        $isi = $("#isi");
        $kantor = this.value;
        $jabatan  =  document.getElementById("selJabatan").value;
        $jabatan = $jabatan == '' ? 'null' : $jabatan;
        $kantor = $kantor == '' ? 'null' : $kantor;
        $lbl = $('#labl').html();
        if (this.value != ''){
            $.ajax({
              url: urlx+"fitnproper/master/ajax/"+$jabatan+"/"+$kantor+"/"+$lbl,
              beforeSend: function() {
                $isi.html($load.html());
              },
              success: function(data) {
                $isi.html(data);
                $("select").select2();
              },
              error: function(data) {
                $isi.html($load2.html());
              }

            })
        }
    });

   // hak akses
  $("#selAll").on('ifChecked', function(){
       $(".checks").iCheck('check');
  });

  $("#selAll").on('ifUnchecked', function(){
       $(".checks").iCheck('uncheck');
  });

  //parents


  $(".parent").on('ifUnchecked', function(){
    classes = this.className;
    idx = classes.split(" ")[2];
    $(".submodul-"+idx).iCheck('uncheck');
  });

  $(".childs").on('ifChecked', function(){
    classes = this.className;
    idx = classes.split(" ")[2].split("-")[1];
    $("."+idx).iCheck('check');
  });

  $(".childs").on('ifUnchecked', function(){
    classes = this.className;
    idx = classes.split(" ")[2].split("-")[1];
    var checked = false;
    $('.submodul-'+idx).each(function(){
      if(this.checked)
        checked = true;
    });

    if (!checked)
      $("."+idx).iCheck('uncheck');
  });

  $(".parent").on('ifClicked', function(){
    classes = this.className;
    idx = classes.split(" ")[2];
    $(".submodul-"+idx).iCheck('check');
  });


  // $(document).ready(function(){
  $("#uploads").click(function(){
      $("#docs").html('<iframe src="http://docs.google.com/gview?url=http://localhost:8000/storage/transkip-1.pdf&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');
  });

  // penilaian makalah
  $(".inps").numeric({ decimal: false, negative: false }, function() { this.value = ""; this.focus(); });
  $('.inps').on('input propertychange', function(){
    var classes = (this.className);
    var persentage = parseInt(classes.split(' ')[3])/100;
    var tot = classes.split(' ')[2];
    var vals = $(this).val();

    if (vals < 0) $(this).val(0);
    if (vals > 100) $(this).val(100);

    var valx = parseInt($(this).val()) > 0 ? parseInt($(this).val()) : 0;
    $('#'+tot).val(Math.round(valx*persentage));
    $('#'+tot+'s').val(Math.round(valx*persentage));

    //CALCULATE TOTAL
    var total = 0;
    $(".totx").each(function(){
      tmp = parseInt($(this).val()) > 0 ? parseInt($(this).val()) : 0;
      total = total + tmp;
    });
    $("#totalx").val(total);
    $("#totalxs").val(total);


  });


});