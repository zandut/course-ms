<?php

$lang['required']			= " %s HARUS diisi !!";
$lang['isset']				= " %s HARUS mempunyai nilai.";
$lang['valid_email']                    = " %s HARUS berupa alamat email.";
$lang['valid_emails']                   = " %s SEMUA HARUS berupa alamat email.";
$lang['valid_url']			= " %s HARUS berupa url yang benar.";
$lang['valid_ip']			= " %s HARUS berupa IP yang benar.";
$lang['min_length']			= " %s mempunyai MIN %s karakter.";
$lang['max_length']			= " %s mempunyai MAKS %s karakter.";
$lang['exact_length']                   = " %s field must be exactly %s characters in length.";
$lang['alpha']				= " %s field may only contain alphabetical characters.";
$lang['alpha_numeric']                  = " %s field may only contain alpha-numeric characters.";
$lang['alpha_dash']			= " %s field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= " %s field must contain only numbers.";
$lang['is_numeric']			= " %s field must contain only numeric characters.";
$lang['integer']			= " %s field must contain an integer.";
$lang['regex_match']                    = " %s field is not in the correct format.";
$lang['matches']			= " %s TIDAK SAMA dengan %s.";
$lang['is_unique'] 			= " %s field must contain a unique value.";
$lang['is_natural']			= " %s field must contain only positive numbers.";
$lang['is_natural_no_zero']             = " %s field must contain a number greater than zero.";
$lang['decimal']			= " %s field must contain a decimal number.";
$lang['less_than']			= " %s field must contain a number less than %s.";
$lang['greater_than']                   = " %s field must contain a number greater than %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */