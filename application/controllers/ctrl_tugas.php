<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ctrl_tugas extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('model_ambil_matkul','',TRUE);
        $this->load->model('model_chapter','',TRUE);
        $this->load->model('model_tugas','',TRUE);
        $this->load->model('model_mahasiswa','',TRUE);
        
    }
    
    private $success = FALSE;
    private $error = FALSE;
    
    public function getSuccess() {
        return $this->success;
    }

    public function getError() {
        return $this->error;
    }

    public function setSuccess($success) {
        $this->success = $success;
    }

    public function setError($error) {
        $this->error = $error;
    }

    function secure($url) {
        $data = $this->mza_secureurl->setSecureUrl_decode($url);
        if ($data != false) {
            if (method_exists($this, trim($data['function']))) {
                if (!empty($data['params'])) {
                    return call_user_func_array(array($this, trim($data['function'])), $data['params']);
                } else {
                    return $this->$data['function']();
                }
            }
        }
        show_404();
    }
        
    public function index() {
        $session = $this->session->userdata('session_ms');
        if ($session == TRUE)
        {
            $mhs = $this->model_mahasiswa->getData($session);
            $this->data['nama'] = $session;
            $this->data['nama_depan'] = $mhs->NAMA;
            
            $this->data['success'] = $this->getSuccess();
            $this->data['error'] = $this->getError();
            
            $this->data['title_pages'] = "Course-MS | Tugas Kuliah";
            $this->data['aktif_home'] = '';
            $this->data['aktif_perkuliahan'] = 'active';
            $this->data['aktif_laporan'] = '';
            $this->data['aktif_data'] = '';
            
            $this->data['data_tugas'] = $this->model_tugas->getData();
            $this->data['data_matkul'] = $this->model_ambil_matkul->getMatkul($session);
            $this->data['data_tugas'] = $this->model_tugas->getData();
            
            $this->load->view('templates/header', $this->data);
            $this->load->view('templates/nav', $this->data);
            $this->load->view('contents/tugas_kuliah', $this->data);
            $this->load->view('templates/footer', $this->data);
            
        }
        else
            redirect ($this->mza_secureurl->setSecureUrl_encode('welcome','index'));
    }
    
    public function insertTugas() {
        $this->form_validation->set_rules('id_chapter','CHAPTER', 'required');
        $this->form_validation->set_rules('judul_tugas','JUDUL TUGAS','required');
        $this->form_validation->set_rules('tgl','TANGGAL PENGUMPULAN','required');
        $this->form_validation->set_rules('jam','JAM PENGUMPULAN','required');
        
        if ($this->form_validation->run())
        {
            $tgl = date_format(date_create($_POST['tgl']), "Y-m-d");
            $query = $this->model_tugas->insertTugas($_POST['id_chapter'], $_POST['judul_tugas'], $tgl, $_POST['jam']);
            if ($query)
            {
                $this->setSuccess(TRUE);
            }
            else
                $this->setSuccess (FALSE);
        }
        else
            $this->setError (TRUE);
        
        $this->index();
    }
    
    public function deleteTugas() {
        $query = $this->model_tugas->deleteTugas($_POST['id_tugas']);
        if ($query)
        {
            $this->setSuccess(TRUE);
        }
        else
            $this->setSuccess (FALSE);
        
        $this->index();
    }
    
    public function updateTugas() {
        $data_tugas = $this->model_tugas->getDataById($_POST['id_tugas']);
        $query = $this->model_tugas->updateTugas($data_tugas->ID_CHAPTER, $_POST['judul_tugas'], date_format(date_create($_POST['tgl']), "Y-m-d"), $_POST['jam'], $_POST['nilai'], $_POST['status'], $_POST['id_tugas']);
        if ($query)
        {
            $this->setSuccess(TRUE);
        }
        else
            $this->setSuccess (FALSE);
        
        $this->index();
    }
    
    
}