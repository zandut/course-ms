<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('ERROR');

class ctrl_mahasiswa extends CI_Controller {

    private $error = false;
    private $success = FALSE;
    private $salah_password = FALSE;
   
    function getSalah_password() {
        return $this->salah_password;
    }

    function setSalah_password($salah_password) {
        $this->salah_password = $salah_password;
    }

        
    function getSuccess() {
        return $this->success;
    }

    function setSuccess($success) {
        $this->success = $success;
    }

    
    private $jml = 1;
    public function index($url) {
        
    }

    public function __construct() {
        parent::__construct();
        $this->load->model('model_mahasiswa', '', TRUE);
        $this->load->model('model_tugas', '', TRUE);
        $this->load->model('model_mata_kuliah', '', TRUE);
        $this->load->model('model_ambil_matkul', '', TRUE);
        $this->load->model('model_kuis', '', TRUE);
        
        
    }
    
    public function gotoHome() {
        if ($this->session->userdata('session_ms') == TRUE) {

            $username = $this->session->userdata('session_ms');
            $user = $this->model_mahasiswa->getData($username);
            $this->data['nama'] = $user->NIM;
            $this->data['nama_depan'] = $user->NAMA;
            $this->data['success'] = FALSE;
            $this->data['error'] = FALSE;
            
            $this->data['title_pages'] = "Course-MS | Dashboard";
            $this->data['aktif_home'] = 'active';
            $this->data['aktif_perkuliahan'] = '';
            $this->data['aktif_laporan'] = '';
            $this->data['aktif_data'] = '';
//            $this->data['aktif_home'] = 'active';
            
            $this->data['jml_tugas'] = $this->model_tugas->getJumlahTugasByStatus('0');
            $this->data['jml_matkul'] = $this->model_ambil_matkul->getJumlahMatkul($user->NIM);
            $this->data['tugas_terdekat'] = $this->model_tugas->getTugasTerdekat($user->NIM);
            $this->data['kuis_terdekat'] = $this->model_kuis->getKuisTerdekat($user->NIM);
            

            $this->load->view('templates/header', $this->data);
            $this->load->view('templates/nav', $this->data);
            $this->load->view('contents/home', $this->data);
            $this->load->view('templates/footer', $this->data);
        } else {
            redirect($this->mza_secureurl->setsecureurl_encode('welcome','index'));
        }
    }

    public function berhasilLogin() {

        $data = $this->model_mahasiswa->getData($_POST['userid']);
        if ($data != NULL)
        {
            if ($data->PASSWORD == md5($_POST['password']))
            {
                $this->setError(FALSE);
                $this->session->set_userdata('session_ms', $data->NIM);
            }
            else
                $this->setError (TRUE);
        }
        else
            $this->setError (TRUE);
        

        $this->data['error'] = $this->getError();
        if ($this->getError()) {

            $this->data['title'] = "Course-MS | Sign In";
            $this->data['header_title'] = "Login";
            $this->load->view('templates/header_login', $this->data);
            $this->load->view('contents/login', $this->data);
            $this->load->view('templates/footer_login');
        } else
        {
            
            $this->gotoKonfirmasi ();
        }
    }
    
    function getJml() {
        return $this->jml;
    }

    function setJml($jml) {
        $this->jml = $jml;
    }

        
    public function gotoKonfirmasi() {
        
        if ($this->session->userdata('session_ms') == TRUE) {
            
            $this->data['error'] = $this->getError();
            $this->data['jml'] = $this->getJml();
            $this->data['title'] = "Course-MS | Konfirmasi Kode";
            $this->data['header_title'] = "Konfirmasi Kode";
            $this->load->view('templates/header_login', $this->data);
            $this->load->view('contents/konfirmasi_kode', $this->data);
            $this->load->view('templates/footer_login');
        
        }
        else
            
            redirect($this->mza_secureurl->setsecureurl_encode('welcome','index'));
    }
    
    public function konfirmasi_kode() {
        
        if ($this->session->userdata('session_ms') == TRUE) {
            $username = $this->session->userdata('session_ms');
            $user = $this->model_mahasiswa->getData($username);
            if ($user->KODE_AKTIFASI == md5($_POST['kode']))
            {
                $this->gotoHome();
                
            }
            else
            {
                
                $this->setError(TRUE);
                $this->setJml($this->getJml() - 1);
                if ($this->getJml() == 0)
                {
                    $this->logOut();
                }
                else
                {
                    $this->gotoKonfirmasi();
                }
                
            }
            
        }
        else
            
            redirect ($this->mza_secureurl->setSecureUrl_encode('welcome','index'));
    }

    
    public function gotoProfile() {
        if ($this->session->userdata('session_ms') == TRUE)
        {
            $username = $this->session->userdata('session_ms');
            $user = $this->model_mahasiswa->getData($username);
            $this->data['nama'] = $user->NIM;
            $this->data['nama_depan'] = $user->NAMA;
            $this->data['alamat'] = $user->ALAMAT;
            $this->data['success'] = $this->getSuccess();
            $this->data['error'] = $this->getError();
            $this->data['salah'] = $this->getSalah_password();
            
            $this->data['title_pages'] = "Course-MS | Profile";
            $this->data['aktif_home'] = 'active';
            $this->data['aktif_perkuliahan'] = '';
            $this->data['aktif_laporan'] = '';
            $this->data['aktif_data'] = '';
            
            $this->load->view('templates/header', $this->data);
            $this->load->view('templates/nav', $this->data);
            $this->load->view('contents/profile', $this->data);
            $this->load->view('templates/footer', $this->data);
        }
        else
            redirect($this->mza_secureurl->setsecureurl_encode('welcome','index'));
    }
    
    public function updateProfile() {
        
        $query = $this->model_mahasiswa->updateMahasiswa($_POST['username'], $_POST['nama'], $_POST['alamat'], $_POST['kode']);
        if ($query == TRUE)
        {
            $this->setSuccess(TRUE);
        }
        
        $this->gotoProfile();
            
    }
    
    public function updatePassword() {
        $this->form_validation->set_rules('password_lama', 'PASSWORD LAMA', 'required');
        $this->form_validation->set_rules('password_baru', 'PASSWORD BARU', 'required');
        $this->form_validation->set_rules('konfirmasi', 'KONFIRMASI PASSWORD', "required|matches[password_baru]");
        
        if ($this->form_validation->run() == TRUE)
        {
            $username = $this->session->userdata('session_ms');
            $data = $this->model_mahasiswa->getData($username);
            if ($data->PASSWORD == md5($_POST['password_lama']))
            {
                $query = $this->model_mahasiswa->updatePassword($username, $_POST['password_baru']);
                if ($query == TRUE)
                {
                    $this->setSuccess(TRUE);
                }
                
            }
            else
            {
                $this->setSalah_password(TRUE);
            }
        }
        else
        {
            $this->setError(TRUE);
        }
        
        $this->gotoProfile();
    }

    public function logOut() {
        $this->session->sess_destroy();
        $this->session->unset_userdata('session_ms');

        redirect(site_url($this->mza_secureurl->setsecureurl_encode('welcome','index')));
    }

    public function getError() {
        return $this->error;
    }

    public function setError($success) {
        $this->error = $success;
    }

    function secure($url) {
        $data = $this->mza_secureurl->setSecureUrl_decode($url);
        if ($data != false) {
            if (method_exists($this, trim($data['function']))) {
                if (!empty($data['params'])) {
                    return call_user_func_array(array($this, trim($data['function'])), $data['params']);
                } else {
                    return $this->$data['function']();
                }
            }
        }
        show_404();
    }

}
