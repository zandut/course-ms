<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('model_mahasiswa','',TRUE);
    }
    public function index() {

        if ($this->session->userdata('session_ms') == FALSE) {

            $this->data['title'] = "Course-MS | Sign In";
            $this->data['header_title'] = "Login";
            $this->load->view('templates/header_login', $this->data);
            $this->load->view('contents/login', $this->data);
            $this->load->view('templates/footer_login');
        } else {

            redirect($this->mza_secureurl->setSecureUrl_encode('ctrl_mahasiswa', 'gotoHome'));
        }
    }

    function secure($url) {
        $data = $this->mza_secureurl->setSecureUrl_decode($url);
        if ($data != false) {
            if (method_exists($this, trim($data['function']))) {
                if (!empty($data['params'])) {
                    return call_user_func_array(array($this, trim($data['function'])), $data['params']);
                } else {
                    return $this->$data['function']();
                }
            }
        }
        show_404();
    }
    
    public function error_404() {
        $session = $this->session->userdata('session_ms');
        if ($session == TRUE)
        {
            $mhs = $this->model_mahasiswa->getData($session);
            
            $this->data['nama'] = $mhs->NIM;
            $this->data['nama_depan'] = $mhs->NAMA;
            
            $this->load->view('templates/header', $this->data);
            $this->load->view('templates/nav', $this->data);
            
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */