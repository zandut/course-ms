<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ctrl_jenis_ujian extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('model_mahasiswa', '', TRUE);
        $this->load->model('model_jenis_ujian', '', TRUE);
        
    }
    
    private $success = FALSE;
    private $error = FALSE;
    
    public function getSuccess() {
        return $this->success;
    }

    public function getError() {
        return $this->error;
    }

    public function setSuccess($success) {
        $this->success = $success;
    }

    public function setError($error) {
        $this->error = $error;
    }

    function secure($url) {
        $data = $this->mza_secureurl->setSecureUrl_decode($url);
        if ($data != false) {
            if (method_exists($this, trim($data['function']))) {
                if (!empty($data['params'])) {
                    return call_user_func_array(array($this, trim($data['function'])), $data['params']);
                } else {
                    return $this->$data['function']();
                }
            }
        }
        show_404();
    }
        
    public function index() {
        $nim = $this->session->userdata('session_ms');
        if ($nim == TRUE)
        {
            $user = $this->model_mahasiswa->getData($nim);
            $this->data['nama'] = $user->NIM;
            $this->data['nama_depan'] = $user->NAMA;
            
            $this->data['success'] = $this->getSuccess();
            $this->data['error'] = $this->getError();
            
            $this->data['title_pages'] = "Course-MS | Data Jenis Ujian";
            $this->data['aktif_home'] = '';
            $this->data['aktif_perkuliahan'] = '';
            $this->data['aktif_laporan'] = '';
            $this->data['aktif_data'] = 'active';
            
            $this->data['data_jenis_ujian'] = $this->model_jenis_ujian->getData();
            
            $this->load->view('templates/header', $this->data);
            $this->load->view('templates/nav', $this->data);
            $this->load->view('contents/jenis_ujian', $this->data);
            $this->load->view('templates/footer', $this->data);
        }
        else
        {
            redirect($this->mza_secureurl->setSecureUrl_encode('welcome','index'));
        }
    }
    
    public function insertJenisUjian() {
        $query = $this->model_jenis_ujian->insertJenisUjian($_POST['jenis']);
        if ($query == TRUE)
        {
            $this->setSuccess(TRUE);
        }
        
        $this->index();
    }
    
    public function updateJenisUjian() {
        $query = $this->model_jenis_ujian->updateJenisUjian($_POST['id_jenis'], $_POST['jenis']);
        if ($query == TRUE)
        {
            $this->setSuccess(TRUE);
        }
        
        $this->index();
    }
    
    public function deleteJenisUjian() {
        $query = $this->model_jenis_ujian->deleteJenisUjian($_POST['id_jenis']);
        if ($query == TRUE)
        {
            $this->setSuccess(TRUE);
        }
        
        $this->index();
    }
    
    public function getJenisUjian() {
        $id_jenis = ($_GET['id']);
        $data_jenis = $this->model_jenis_ujian->getDataById($id_jenis);
        echo "<input type='text' name='jenis' value='".$data_jenis->NAMA_JENIS."' class='form-control' />";
        echo "<input type='text' name='id_jenis' value='".$data_jenis->ID_JENIS."' class='form-control hidden' />";
        
    }
}