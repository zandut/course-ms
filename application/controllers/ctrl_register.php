<?php

if (!defined('BASEPATH'))
    exit('No Direct');

class ctrl_register extends CI_Controller {

    private $error = false;
    private $succes = false;
    private $exception = "";

    public function getException() {
        return $this->exception;
    }

    public function setException($exception) {
        $this->exception = $exception;
    }

    public function __construct() {
        parent::__construct();
        $this->load->model('model_mahasiswa', '', TRUE);
    }

    public function getSucces() {
        return $this->succes;
    }

    public function setSucces($succes) {
        $this->succes = $succes;
    }

    public function getError() {
        return $this->error;
    }

    public function setError($error) {
        $this->error = $error;
    }

    public function index() {

        $this->data['error'] = $this->getError();
        $this->data['success'] = $this->getSucces();
        $this->data['title'] = "Course-MS | Registrasi User";
        $this->data['header_title'] = "Registrasi";
        $this->data['exception'] = $this->getException();
        $this->load->view('templates/header_login', $this->data);
        $this->load->view('contents/register', $this->data);
        $this->load->view('templates/footer_login');
    }

    public function signUp() {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('userid', 'NIM', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('password1', 'Konfirmasi Password', 'required|matches[password]');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');


        if ($this->form_validation->run() == FALSE) {
            $this->setError(true);
            
        } else {

//            $this->load->library('input');
            $query = $this->model_mahasiswa->insertMahasiswa($_POST['userid'], $_POST['nama'], $_POST['alamat'], $_POST['password']);
            if ($query == TRUE)
            {
                $this->setSucces(TRUE);
            }

        }
        
        $this->index();
    }

    function secure($url) {
        $data = $this->mza_secureurl->setSecureUrl_decode($url);
        if ($data != false) {
            if (method_exists($this, trim($data['function']))) {
                if (!empty($data['params'])) {
                    return call_user_func_array(array($this, trim($data['function'])), $data['params']);
                } else {
                    return $this->$data['function']();
                }
            }
        }
        show_404();
    }

}
