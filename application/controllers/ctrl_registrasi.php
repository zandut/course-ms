<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ctrl_registrasi extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        
        $this->load->model('model_mahasiswa','',TRUE);
        $this->load->model('model_dosen', '', TRUE);
        $this->load->model('model_mata_kuliah', '', TRUE);
        $this->load->model('model_ambil_matkul','',TRUE);
        $this->load->model('model_tahun_semester','',TRUE);
        $this->load->model('model_chapter','',TRUE);
    }
    
    private $success;
    private $error;
    
    public function getSuccess() {
        return $this->success;
    }

    public function getError() {
        return $this->error;
    }

    public function setSuccess($success) {
        $this->success = $success;
    }

    public function setError($error) {
        $this->error = $error;
    }
    
    function secure($url) {
        $data = $this->mza_secureurl->setSecureUrl_decode($url);
        if ($data != false) {
            if (method_exists($this, trim($data['function']))) {
                if (!empty($data['params'])) {
                    return call_user_func_array(array($this, trim($data['function'])), $data['params']);
                } else {
                    return $this->$data['function']();
                }
            }
        }
        show_404();
    }

        
    public function index() {
        $session = $this->session->userdata('session_ms');
        if ($session == TRUE)
        {
            $mhs = $this->model_mahasiswa->getData($session);
            $this->data['nama'] = $mhs->NIM;
            $this->data['nama_depan'] = $mhs->NAMA;
            
            $this->data['success'] = $this->getSuccess();
            $this->data['error'] = $this->getError();
            
            $this->data['title_pages'] = "Course-MS | Registrasi";
            $this->data['aktif_home'] = '';
            $this->data['aktif_perkuliahan'] = 'active';
            $this->data['aktif_laporan'] = '';
            $this->data['aktif_data'] = '';
            
            $this->data['data_dosen'] = $this->model_dosen->getData();
            $matkul = $this->model_ambil_matkul->getMatkul($session);
            $this->data['data_matkul'] = $matkul;
            
            $jumlah_chapter = array();
            
            foreach ($matkul as $data) {
                $jumlah_chapter[$data->KODE_MATKUL] = $this->model_chapter->getJumlahChapter($data->KODE_MATKUL);
                
            }
            
            $this->data['jumlah_chapter'] = $jumlah_chapter;
            
            $this->load->view('templates/header', $this->data);
            $this->load->view('templates/nav', $this->data);
            $this->load->view('contents/registrasi', $this->data);
            $this->load->view('templates/footer', $this->data);
            
        }
        else
        {
            redirect($this->mza_secureurl->setSecureUrl_encode('welcome','index'));
        }
    }
    
    public function insertMatkul($nim) {
        $this->form_validation->set_rules('kode_matkul','KODE MATKUL','required');
        $this->form_validation->set_rules('kode_dosen', 'KODE DOSEN', 'required');
        $this->form_validation->set_rules('nama_matkul','NAMA MATKUL', 'required');
        
        if ($this->form_validation->run() == TRUE)
        {
            $tahun_semester = $this->model_tahun_semester->getDataByIsAktif('1');
            $query = $this->model_mata_kuliah->insertMatkul($_POST['kode_matkul'], $_POST['kode_dosen'], $tahun_semester->ID_TAHUN_SEMESTER, $_POST['nama_matkul']);
            if ($query == TRUE)
            {
                $query1 = $this->model_ambil_matkul->ambil_matkul($_POST['kode_matkul'], $nim);
                if ($query1 == TRUE)
                    $this->setSuccess(TRUE);
            }
            else
                $this->setSuccess(FALSE);

        }
        else
            $this->setError(TRUE);
        
         $this->index();
    }
    
    public function updateMatkul() {
        $query = $this->model_mata_kuliah->updateMatkul($_POST['kode_matkul'], $_POST['kode_dosen'], $_POST['nama_matkul']);
        if ($query)
        {
            $this->setSuccess(TRUE);
        }
        else
            $this->setSuccess (FALSE);
        
        $this->detail_matkul($_POST['kode_matkul']);
    }
    
    public function deleteMatkul() {
        
        $query = $this->model_mata_kuliah->deleteMatkul($_POST['kode']);
        if ($query == TRUE)
            $this->setSuccess(TRUE);

        else
            $this->setSuccess (FALSE);
        
        $this->index();
    }
    
    public function deleteChapter() {
        $query = $this->model_chapter->deleteChapter($_POST['id']);
        if ($query)
        {
            $this->setSuccess(TRUE);
        }
        else
           $this->setSuccess (FALSE);
        
       $this->detail_matkul($_POST['kode']);
    }
    
    public function insertChapter() {
        $this->form_validation->set_rules('kode_matkul','KODE MATKUL','required');
        $this->form_validation->set_rules('nama_chapter','NAMA CHAPTER', 'required');
        
        if ($this->form_validation->run() == TRUE)
        {
            $query = $this->model_chapter->insertChapter($_POST['kode_matkul'],$_POST['nama_chapter'],'');
            if ($query == TRUE)
                $this->setSuccess (TRUE);
            else 
                $this->setSuccess (FALSE);
        }
        else
            $this->setError (TRUE);
        
        $this->detail_matkul($_POST['kode_matkul']);
        
    }
    
    public function editChapter() {
        $chapter = $this->model_chapter->getDataById($_POST['id_chapter']);
        $query = $this->model_chapter->updateChapter($chapter->KODE_MATKUL, $_POST['nama_chapter'], $chapter->CATATAN, $_POST['id_chapter']);
        if ($query == TRUE)
                $this->setSuccess (TRUE);
            else 
                $this->setSuccess (FALSE);
            
        $this->detail_matkul($chapter->KODE_MATKUL);
        
    }
    
    public function detail_matkul($kode_matkul) {
        $session = $this->session->userdata('session_ms');
        if ($session == TRUE)
        {
            $mhs = $this->model_mahasiswa->getData($session);
            $this->data['nama'] = $mhs->NIM;
            $this->data['nama_depan'] = $mhs->NAMA;
            
            $this->data['success'] = $this->getSuccess();
            $this->data['error'] = $this->getError();
            
            $this->data['title_pages'] = "Course-MS | Detail Kuliah";
            $this->data['aktif_home'] = '';
            $this->data['aktif_perkuliahan'] = 'active';
            $this->data['aktif_laporan'] = '';
            $this->data['aktif_data'] = '';
            
            $this->data['data_chapter'] = $this->model_chapter->getDataByKodeMatkul($kode_matkul);
            $this->data['data_matkul'] = $this->model_ambil_matkul->getMatkulByKode($session, $kode_matkul);
            $this->data['data_dosen'] = $this->model_dosen->getData();
            
            $this->load->view('templates/header', $this->data);
            $this->load->view('templates/nav', $this->data);
            $this->load->view('contents/detail_kuliah', $this->data);
            $this->load->view('templates/footer', $this->data);
            
        }
        else
        {
            redirect($this->mza_secureurl->setSecureUrl_encode('welcome','index'));
        }
    }
    
    
}