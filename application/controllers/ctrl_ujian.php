<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ctrl_ujian extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('model_mahasiswa', '', TRUE);
        $this->load->model('model_ujian', '', TRUE);
        $this->load->model('model_jenis_ujian','',TRUE);
        $this->load->model('model_ambil_matkul', '', TRUE);
        $this->load->model('model_ujian_matkul', '', TRUE);
    }
    
    private $success = FALSE;
    private $error = FALSE;
    
    public function getSuccess() {
        return $this->success;
    }

    public function getError() {
        return $this->error;
    }

    public function setSuccess($success) {
        $this->success = $success;
    }

    public function setError($error) {
        $this->error = $error;
    }

    function secure($url) {
        $data = $this->mza_secureurl->setSecureUrl_decode($url);
        if ($data != false) {
            if (method_exists($this, trim($data['function']))) {
                if (!empty($data['params'])) {
                    return call_user_func_array(array($this, trim($data['function'])), $data['params']);
                } else {
                    return $this->$data['function']();
                }
            }
        }
        show_404();
    }

    public function index() {
        $session = $this->session->userdata('session_ms');
        if ($session)
        {
            $user = $this->model_mahasiswa->getData($session);
            $this->data['nama'] = $user->NIM;
            $this->data['nama_depan'] = $user->NAMA;
            
            $this->data['success'] = $this->getSuccess();
            $this->data['error'] = $this->getError();
            
            $this->data['title_pages'] = "Course-MS | Ujian Mata Kuliah";
            $this->data['aktif_home'] = '';
            $this->data['aktif_perkuliahan'] = 'active';
            $this->data['aktif_laporan'] = '';
            $this->data['aktif_data'] = '';
            
            $this->data['data_ujian'] = $this->model_ujian_matkul->getDataByNim($session);
            
            $this->data['data_matkul'] = $this->model_ambil_matkul->getMatkul($session);
            $this->data['data_jenis_ujian'] = $this->model_jenis_ujian->getData();
            
            $this->load->view('templates/header', $this->data);
            $this->load->view('templates/nav', $this->data);
            $this->load->view('contents/ujian', $this->data);
            $this->load->view('templates/footer', $this->data);
        }
        else
            redirect ($this->mza_secureurl->setSecureUrl_encode('welcome','index'));
    }
    
    public function insertUjian() {
        $session = $this->session->userdata('session_ms');
        $data_matkul = $this->model_ambil_matkul->getMatkulByKode($session, $_POST['kode_matkul']);
        
        $query = $this->model_ujian->insertUjian($_POST['id_jenis_ujian'], $data_matkul->NAMA_MATKUL, $_POST['tgl'], $_POST['jam'], $_POST['keterangan']);
        if ($query)
        {
            $data_ujian = $this->model_ujian->getLastData();
            $query1 = $this->model_ujian_matkul->insertUjianMatkul($data_matkul->ID_AMBIL, $data_ujian->ID_UJIAN);
            if ($query1)
            {
                $this->setSuccess(TRUE);
            }
            else
                $this->setSuccess (FALSE);
        }
        else {
            $this->setSuccess(FALSE);
        }
        
        $this->index();
        
        
    }
    
    public function updateUjian() {
        $session = $this->session->userdata('session_ms');
        $data_matkul = $this->model_ambil_matkul->getMatkulByKode($session, $_POST['kode_matkul']);
        
        $query = $this->model_ujian->updateUjian($_POST['id_jenis_ujian'], $data_matkul->NAMA_MATKUL, $_POST['tgl'], $_POST['jam'], $_POST['keterangan'], $_POST['id_ujian']);
        if ($query)
        {
            $data_ujian = $this->model_ujian->getDataById($_POST['id_ujian']);
            $query1 = $this->model_ujian_matkul->updateUjianMatkul($_POST['nilai'],$data_matkul->ID_AMBIL, $data_ujian->ID_UJIAN);
            if ($query1)
            {
                $this->setSuccess(TRUE);
            }
            else
                $this->setSuccess (FALSE);
            
        }
        else
        {
            $this->setSuccess(FALSE);
        }
        
        $this->index();
    }
    
    public function deleteUjian() {
        $query = $this->model_ujian->deleteUjian($_POST['id_ujian']);
        if ($query)
        {
            $this->setSuccess(TRUE);
        }
        else
        {
            $this->setSuccess(FALSE);
        }
        
        $this->index();
        
    }
}