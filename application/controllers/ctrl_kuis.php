<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ctrl_kuis extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('model_mahasiswa','',TRUE);
        $this->load->model('model_kuis','',TRUE);
        $this->load->model('model_ambil_matkul','',TRUE);
        
        
    }
    
    function secure($url) {
        $data = $this->mza_secureurl->setSecureUrl_decode($url);
        if ($data != false) {
            if (method_exists($this, trim($data['function']))) {
                if (!empty($data['params'])) {
                    return call_user_func_array(array($this, trim($data['function'])), $data['params']);
                } else {
                    return $this->$data['function']();
                }
            }
        }
        show_404();
    }
    
    private $success = FALSE;
    private $error = FALSE;
    
    public function getSuccess() {
        return $this->success;
    }

    public function getError() {
        return $this->error;
    }

    public function setSuccess($success) {
        $this->success = $success;
    }

    public function setError($error) {
        $this->error = $error;
    }

    public function index() {
        $session = $this->session->userdata('session_ms');
        if ($session)
        {
            $user = $this->model_mahasiswa->getData($session);
            $this->data['nama'] = $user->NIM;
            $this->data['nama_depan'] = $user->NAMA;
            
            $this->data['success'] = $this->getSuccess();
            $this->data['error'] = $this->getError();
            
            $this->data['title_pages'] = "Course-MS | Kuis";
            $this->data['aktif_home'] = '';
            $this->data['aktif_perkuliahan'] = 'active';
            $this->data['aktif_laporan'] = '';
            $this->data['aktif_data'] = '';
            
            $this->data['data_kuis'] = $this->model_kuis->getData();
            $this->data['data_matkul'] = $this->model_ambil_matkul->getMatkul($session);
            
            
            $this->load->view('templates/header', $this->data);
            $this->load->view('templates/nav', $this->data);
            $this->load->view('contents/kuis', $this->data);
            $this->load->view('templates/footer', $this->data);
        }
        else
            redirect ($this->mza_secureurl->setSecureUrl_encode('welcome','index'));
    }
    
    public function insertKuis() {
        $session = $this->session->userdata('session_ms');
        $matkul = $this->model_ambil_matkul->getMatkulByKode($session, $_POST['kode_matkul']);
        
        $query = $this->model_kuis->insertKuis($_POST['id_chapter'], "Kuis ".$matkul->NAMA_MATKUL, $_POST['tgl'], $_POST['jam'], $_POST['keterangan'], '0');
        if ($query)
        {
            $this->setSuccess(TRUE);
        }
        else
            $this->setSuccess (FALSE);
        
        $this->index();
    }
    
    public function updateKuis() {
        $query = $this->model_kuis->updateKuis($_POST['id_chapter'], $_POST['nama_kuis'], $_POST['tgl'], $_POST['jam'], $_POST['keterangan'], $_POST['nilai'], $_POST['id_kuis']);
        if ($query)
        {
            $this->setSuccess(TRUE);
        }
        else
            $this->setSuccess (FALSE);
        
        $this->index();
    }
    
    public function deleteKuis() {
        $query = $this->model_kuis->deleteKuis($_POST['id_kuis']);
        if ($query)
        {
            $this->setSuccess(TRUE);
        }
        else
            $this->setSuccess (FALSE);
        
        $this->index();
    }
    
}