<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ctrl_tahun extends CI_Controller
{
    private $error = FALSE;
    private $success = FALSE;
    
    public function getError() {
        return $this->error;
    }

    public function getSuccess() {
        return $this->success;
    }

    public function setError($error) {
        $this->error = $error;
    }

    public function setSuccess($success) {
        $this->success = $success;
    }
    
    function secure($url) {
        $data = $this->mza_secureurl->setSecureUrl_decode($url);
        if ($data != false) {
            if (method_exists($this, trim($data['function']))) {
                if (!empty($data['params'])) {
                    return call_user_func_array(array($this, trim($data['function'])), $data['params']);
                } else {
                    return $this->$data['function']();
                }
            }
        }
        show_404();
    }

    public function __construct() {
        parent::__construct();
        $this->load->model('model_tahun','',TRUE);
        $this->load->model('model_mahasiswa','',TRUE);
        $this->load->model('model_semester','',TRUE);
        $this->load->model('model_tahun_semester','',TRUE);
    }
    
    public function index() {
        $username = $this->session->userdata('session_ms');
        if ($username == TRUE)
        {
            $user = $this->model_mahasiswa->getData($username);
            $this->data['nama'] = $user->NIM;
            $this->data['nama_depan'] = $user->NAMA;
            
            $this->data['success'] = $this->getSuccess();
            $this->data['error'] = $this->getError();
            
            $this->data['title_pages'] = "Course-MS | Data Tahun Akademik";
            $this->data['aktif_home'] = '';
            $this->data['aktif_perkuliahan'] = '';
            $this->data['aktif_laporan'] = '';
            $this->data['aktif_data'] = 'active';
            
            $this->data['data_tahun'] = $this->model_tahun->getData();
            $this->data['data_semester'] = $this->model_semester->getData();
            
            $this->data['data_tahun_semester'] = $this->model_tahun_semester->getDataByIsAktif('1');
            
            $this->load->view('templates/header', $this->data);
            $this->load->view('templates/nav', $this->data);
            $this->load->view('contents/tahun_akademik', $this->data);
            $this->load->view('templates/footer', $this->data);
        }
        else
        {
            redirect($this->mza_secureurl->setsecureUrl_encode('welcome','index'));
        }
    }
    
    public function insertTahun() {
        $query = $this->model_tahun->insertTahun($_POST['tahun']);
        if ($query == TRUE)
        {
            $semester = $this->model_semester->getData();
            foreach ($semester as $data) {
                $query1 = $this->model_tahun_semester->insertTahunSemester($_POST['tahun'], $data->ID_SEMESTER);
            }
            if ($query1 == TRUE)
            {
                $this->setSuccess(TRUE);
            }
        }
        else
            $this->setSuccess (FALSE);
        
        $this->index();
    }
    
    public function deleteTahun() {
        $query = $this->model_tahun->deleteTahun($_POST['tahun']);
        if ($query == TRUE)
        {
            $this->setSuccess(TRUE);
        }
        else
            $this->setSuccess (FALSE);
        
        $this->index();
    }
    
    public function setTahunAkademik() {
        $tahun_semester = $this->model_tahun_semester->getDataByIsAktif('1');
        
        if ($tahun_semester != NULL) {
            $query1 = $this->model_tahun_semester->updateTahunSemester($tahun_semester->TAHUN, $tahun_semester->ID_SEMESTER, '0');
        }

        $query = $this->model_tahun_semester->updateTahunSemester($_POST['tahun'], $_POST['semester'], '1');
        if ($query == TRUE)
        {
            $this->setSuccess(TRUE);
        }
        else
            $this->setSuccess (FALSE);
        
        $this->index();
    }
}