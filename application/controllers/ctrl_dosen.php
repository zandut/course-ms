<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ctrl_dosen extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('model_mahasiswa','',TRUE);
        $this->load->model('model_dosen','',TRUE);
        
    }
    
    private $success;
    private $error;
    
    public function getSuccess() {
        return $this->success;
    }

    public function getError() {
        return $this->error;
    }

    public function setSuccess($success) {
        $this->success = $success;
    }

    public function setError($error) {
        $this->error = $error;
    }

        
    function secure($url) {
        $data = $this->mza_secureurl->setSecureUrl_decode($url);
        if ($data != false) {
            if (method_exists($this, trim($data['function']))) {
                if (!empty($data['params'])) {
                    return call_user_func_array(array($this, trim($data['function'])), $data['params']);
                } else {
                    return $this->$data['function']();
                }
            }
        }
        show_404();
    }
    
    public function index() {
        
        $session = $this->session->userdata('session_ms');
        if ($session == TRUE)
        {
            $user = $this->model_mahasiswa->getData($session);
            $this->data['nama'] = $user->NIM;
            $this->data['nama_depan'] = $user->NAMA;
            
            $this->data['success'] = $this->getSuccess();
            $this->data['error'] = $this->getError();
            
            $this->data['title_pages'] = "Course-MS | Data Dosen";
            $this->data['aktif_home'] = '';
            $this->data['aktif_perkuliahan'] = '';
            $this->data['aktif_laporan'] = '';
            $this->data['aktif_data'] = 'active';
            
            $this->data['data_dosen'] = $this->model_dosen->getData();
            
            $this->load->view('templates/header', $this->data);
            $this->load->view('templates/nav', $this->data);
            $this->load->view('contents/dosen', $this->data);
            $this->load->view('templates/footer', $this->data);
        }
        else
            redirect ($this->mza_secureurl->setSecureUrl_encode('welcome','index',array()));
    }
    
    public function insertDosen() {
        $this->form_validation->set_rules('kode_dosen', 'KODE DOSEN', 'required');
        $this->form_validation->set_rules('nama_dosen', 'NAMA DOSEN', 'required');
        $this->form_validation->set_rules('email', 'EMAIL DOSEN', 'required|valid_email');
        $this->form_validation->set_rules('hp', 'NO HP DOSEN', 'required');
        
        if ($this->form_validation->run() == TRUE)
        {
            $query = $this->model_dosen->insertDosen($_POST['kode_dosen'], $_POST['nama_dosen'], $_POST['email'], $_POST['hp']);
            if ($query == TRUE)
            {
                $this->setSuccess(TRUE);
            }
            else
            {
                $this->setSuccess(FALSE);
            }
        }
        else
            $this->setError (TRUE);
        
        $this->index();
    }
    
    public function deleteDosen() {
        $query = $this->model_dosen->deleteDosen($_POST['kode_dosen']);
        if ($query == TRUE)
        {
            $this->setSuccess(TRUE);
        }
        else
            $this->setSuccess (FALSE);
        
        $this->index();
    }
    
    public function updateDosen() {
        $query = $this->model_dosen->updateDosen($_POST['kode_dosen'], $_POST['nama_dosen'], $_POST['email'], $_POST['hp']);
        if ($query == TRUE)
        {
            $this->setSuccess(TRUE);
        }
        else
        {
            $this->setSuccess(FALSE);
        }
        
        $this->index();
    }
    
    public function getDosen() {
        $kode_dosen = $_GET['id'];
        
        $data_dosen = $this->model_dosen->getDataByKode($kode_dosen);
        
        echo "<input type='text' name='kode_dosen' value='".$data_dosen->KODE_DOSEN."' class='form-control hidden' />";
        echo "<div class='form-group'>";
        echo "<label>Nama Dosen</label>";
        echo "<input type='text' name='nama_dosen' value='".$data_dosen->NAMA_DOSEN."' class='form-control' />";
        echo "</div>";
        echo "<div class='form-group'>";
        echo "<label>Email Dosen</label>";
        echo "<input type='text' name='email' value='".$data_dosen->EMAIL."' class='form-control' />";
        echo "</div>";
        echo "<div class='form-group'>";
        echo "<label>No HP Dosen</label>";
        echo "<input type='text' name='hp' value='".$data_dosen->NO_HP."' class='form-control' />";
        echo "</div>";
        
    }
}