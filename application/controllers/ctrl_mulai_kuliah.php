<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ctrl_mulai_kuliah extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('model_ambil_matkul','',TRUE);
        $this->load->model('model_chapter','',TRUE);
        $this->load->model('model_mahasiswa','',TRUE);
        $this->load->model('model_mata_kuliah','',TRUE);
    }
    
    private $success = FALSE;
    private $error = FALSE;
    
    public function getSuccess() {
        return $this->success;
    }

    public function getError() {
        return $this->error;
    }

    public function setSuccess($success) {
        $this->success = $success;
    }

    public function setError($error) {
        $this->error = $error;
    }
    
    function secure($url) {
        $data = $this->mza_secureurl->setSecureUrl_decode($url);
        if ($data != false) {
            if (method_exists($this, trim($data['function']))) {
                if (!empty($data['params'])) {
                    return call_user_func_array(array($this, trim($data['function'])), $data['params']);
                } else {
                    return $this->$data['function']();
                }
            }
        }
        show_404();
    }

    public function index()
    {
        $username = $this->session->userdata('session_ms');
        if ($username == TRUE)
        {
            $mahasiswa = $this->model_mahasiswa->getData($username);
            $this->data['nama'] = $mahasiswa->NIM;
            $this->data['nama_depan'] = $mahasiswa->NAMA;
            
            $this->data['success'] = $this->getSuccess();
            $this->data['error']= $this->getError();
            
            $this->data['title_pages'] = "Course-MS | Mulai Kuliah";
            $this->data['aktif_home'] = '';
            $this->data['aktif_perkuliahan'] = 'active';
            $this->data['aktif_laporan'] = '';
            $this->data['aktif_data'] = '';
            
            $this->data['data_kuliah'] = $this->model_ambil_matkul->getMatkul($username);
            
            $this->load->view('templates/header', $this->data);
            $this->load->view('templates/nav', $this->data);
            $this->load->view('contents/mulai_kuliah', $this->data);
            $this->load->view('templates/footer', $this->data);
        }
        else
            redirect ($this->mza_secureurl->setSecureUrl_encode('welcome','index'));
    }
    
    public function getChapter() {
        
        $kode_matkul = ($_GET['id']);
        
        $chapter = $this->model_chapter->getDataByKodeMatkul($kode_matkul);
        
        echo "<select class='form-control' name='id_chapter'><option value=''>---Pilih Chapter---</option>";
        foreach ($chapter as $data) {
            echo "<option value='".$data->ID_CHAPTER."'>".$data->NAMA_CHAPTER."</option></select>";
        }
    }
    
    public function insertChapter() {
        $this->form_validation->set_rules('kode_matkul','KODE MATKUL','required');
        $this->form_validation->set_rules('nama_chapter','NAMA CHAPTER', 'required');
        
        if ($this->form_validation->run() == TRUE)
        {
            $query = $this->model_chapter->insertChapter($_POST['kode_matkul'],$_POST['nama_chapter'],'');
            if ($query == TRUE)
                $this->setSuccess (TRUE);
            else 
                $this->setSuccess (FALSE);
        }
        else
            $this->setError (TRUE);
        
        $this->index();
        
        
    }
    
    
    
    public function mulaiKuliah() {
        
        $chapter = $this->model_chapter->getDataById($_POST['id_chapter']);
        $query = $this->model_chapter->updateChapter($chapter->KODE_MATKUL, $chapter->NAMA_CHAPTER, $_POST['catatan'], $_POST['id_chapter']);
        if ($query == TRUE)
            $this->setSuccess (TRUE);
        else
            $this->setSuccess (FALSE);


        $this->gotoDetail();
    }
    
    
    
    public function getCatatan() {
        $kode_chapter= ($_GET['id']);
        
        $chapter = $this->model_chapter->getDataById($kode_chapter);
        echo "<textarea id='editor1' name='catatan' rows='20' cols='80'>".$chapter->CATATAN."</textarea>";
    }
    
    public function gotoDetail() {
        $username = $this->session->userdata('session_ms');
        if ($username == TRUE)
        {
            $mahasiswa = $this->model_mahasiswa->getData($username);
            $this->data['nama'] = $mahasiswa->NIM;
            $this->data['nama_depan'] = $mahasiswa->NAMA;
            
            $this->data['success'] = $this->getSuccess();
            $this->data['error']= $this->getError();
            
            $this->data['title_pages'] = "Course-MS | Mulai Kuliah";
            $this->data['aktif_home'] = '';
            $this->data['aktif_perkuliahan'] = 'active';
            $this->data['aktif_laporan'] = '';
            $this->data['aktif_data'] = '';
            $this->form_validation->set_rules('kode_matkul','KODE MATKUL', 'required');
            $this->form_validation->set_rules('id_chapter','ID CHAPTER', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {
                $this->data['data_chapter'] = $this->model_chapter->getDataById($_POST['id_chapter']);
              
                $this->load->view('templates/header', $this->data);
                $this->load->view('templates/nav', $this->data);
                $this->load->view('contents/detail_mulai_kuliah', $this->data);
                $this->load->view('templates/footer', $this->data);
            }
            else
                $this->index ();
        }
        else
            redirect ($this->mza_secureurl->setSecureUrl_encode('welcome','index'));
    }
    
    public function gotoDetailByIdChapter($id_chapter) {
        $username = $this->session->userdata('session_ms');
        if ($username == TRUE)
        {
            $mahasiswa = $this->model_mahasiswa->getData($username);
            $this->data['nama'] = $mahasiswa->NIM;
            $this->data['nama_depan'] = $mahasiswa->NAMA;
            
            $this->data['success'] = $this->getSuccess();
            $this->data['error']= $this->getError();
            
            $this->data['title_pages'] = "Course-MS | Mulai Kuliah";
            $this->data['aktif_home'] = '';
            $this->data['aktif_perkuliahan'] = 'active';
            $this->data['aktif_laporan'] = '';
            $this->data['aktif_data'] = '';
            
            $this->data['data_chapter'] = $this->model_chapter->getDataById($id_chapter);

            $this->load->view('templates/header', $this->data);
            $this->load->view('templates/nav', $this->data);
            $this->load->view('contents/detail_mulai_kuliah', $this->data);
            $this->load->view('templates/footer', $this->data);
            
        }
        else
            redirect ($this->mza_secureurl->setSecureUrl_encode('welcome','index'));
        
    }
}