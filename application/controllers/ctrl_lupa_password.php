<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ctrl_lupa_password extends CI_Controller {

    private $error = false;
    private $succes = false;
    
    

    public function getError() {
        return $this->error;
    }

    public function getSucces() {
        return $this->succes;
    }

    public function setError($error) {
        $this->error = $error;
    }

    public function setSucces($succes) {
        $this->succes = $succes;
    }

    public function __construct() {
        parent::__construct();
        $this->load->model('model_mahasiswa', '', true);
        
    }

    private $username = "";
    
    public function getUsername() {
        return $this->username;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function index() {
        $this->data['error'] = $this->getError();
        $this->data['success'] = $this->getSucces();
        $this->data['title'] = "AplikasiHRD | Lupa Password";
        $this->data['header_title'] = "Lupa Password";
        $this->load->view('templates/header_login', $this->data);
        $this->load->view('contents/lupa_password', $this->data);
        $this->load->view('templates/footer_login');
    }

    public function gotoreset_password() {
        $this->data['error'] = $this->getError();
        $this->data['title'] = "AplikasiHRD | Reset Password";
        $this->data['header_title'] = "Reset Password";
        $this->data['success'] = $this->getSucces();
        $this->data['username'] = $this->getUsername();
        $this->load->view('templates/header_login', $this->data);
        $this->load->view('contents/reset_password', $this->data);
        $this->load->view('templates/footer_login');
    }

    public function checkUsername() {
        $username = $_POST['userid'];
        $num = $this->model_mahasiswa->getData($username);
        if ($num != NULL) {
            $this->setError(false);
            $this->setUsername($username);
            $this->gotoreset_password();
        } else {
            $this->setError(true);
            $this->index();
        }
    }
    
    public function resetPassword($username) {
        $this->form_validation->set_rules('password', 'PASSWORD', 'required');
        $this->form_validation->set_rules('password1','KONFIRMASI PASSWORD', 'required|matches[password]');
        if ($this->form_validation->run() == FALSE)
        {
            $this->setError(TRUE);
        }
        else
        {
            $user = $this->model_mahasiswa->getData($username);
            $query = $this->model_mahasiswa->updateMahasiswa($user->NIM,  $user->NAMA, $user->ALAMAT, $_POST['password'], $user->KODE_AKTIFASI);
            if ($query == TRUE)
            {
                $this->setSucces(TRUE);
            }
            else
                $this->setSucces (FALSE);
        
        }
        $this->gotoreset_password();
    }

    function secure($url) {
        $data = $this->mza_secureurl->setSecureUrl_decode($url);
        if ($data != false) {
            if (method_exists($this, trim($data['function']))) {
                if (!empty($data['params'])) {
                    return call_user_func_array(array($this, trim($data['function'])), $data['params']);
                } else {
                    return $this->$data['function']();
                }
            }
        }
        show_404();
    }

}
