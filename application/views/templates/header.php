<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $title_pages ?></title>

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo base_url('assets/css/ionicons.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo base_url('assets/css/datepicker/datepicker3.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/morris/morris.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css//ionicons.min.css')?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url('assets/css/AdminLTE.css'); ?>" rel="stylesheet" type="text/css" />

        <!-- Bootstrap time Picker -->
        <link href="<?= base_url('assets/css/timepicker/jquery.timepicker.css'); ?>" rel="stylesheet" type="text/css"/>

        <link href="<?= base_url('assets/css/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url('assets/css/datatables/dataTables.tableTools.css'); ?>" rel="stylesheet" type="text/css"/>

         <!--<link href="<?= base_url('assets/css/datatables/jquery.dataTables.css'); ?>" rel="stylesheet" type="text/css"/>-->
        <link href="<?= base_url('assets/css/iCheck/all.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/jQueryUI/jquery-ui-1.10.3.custom.css') ?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?= base_url('assets/js/date.js')?>"></script>
        <style>
            .datepicker{z-index:1151 !important;}
        </style>
        
        <link type="text/css" rel="stylesheet" href="<?= base_url('assets/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')?>" />
        
        <link type="text/css" rel="stylesheet" href="<?= base_url('assets/css/countdown/flipclock.css')?>" />


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <header class="header">
            <a href="<?= base_url() ?>" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Course-MS
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">

                        <!-- User Account: style can be found in dropdown.less -->
                        
                        
                        
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                
                                <span ><i class="glyphicon glyphicon-user "></i> <?= $nama ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="<?= base_url('assets/img/avatar5.png') ?>" class="img-circle" alt="User Image" />
                                    <p>
                                        <?= $nama ?> - <?= $nama_depan ?>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">

                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?= site_url($this->mza_secureurl->setSecureUrl_encode('ctrl_mahasiswa','gotoProfile'))?>" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_mahasiswa', 'logOut')) ?>" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </nav>
        </header>