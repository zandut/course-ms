<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?= base_url('assets/img/avatar5.png')?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>Oi, <?= ($nama_depan) ?></p>

                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu nav nav-stacked">
                <li class="<?= $aktif_home?>">
                    <a href="<?= base_url(); ?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                
                <li class="treeview <?= $aktif_perkuliahan?>">
                    <a href="#">
                        <i class="fa fa-book"></i>
                        <span>Perkuliahan</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_registrasi','index')) ?>"><i class="fa fa-angle-double-right"></i>Registrasi</a></li>
                        <li><a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_mulai_kuliah','index')) ?>"><i class="fa fa-angle-double-right"></i>Mulai Kuliah</a></li>
<!--                        <li><a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_jadwal','index'))?>"><i class="fa fa-angle-double-right"></i>Jadwal Kuliah</a></li>-->
                        <li><a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_tugas','index'))?>"><i class="fa fa-angle-double-right"></i>Tugas Kuliah</a></li>
                        
                        <li><a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_kuis','index'))?>"><i class="fa fa-angle-double-right"></i>Kuis</a></li>
                        <li><a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_ujian','index'))?>"><i class="fa fa-angle-double-right"></i>Ujian</a></li>
                        <li><a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_nilai','index'))?>"><i class="fa fa-angle-double-right"></i>Lihat Nilai</a></li>
                    </ul>
                </li>
                <li class="treeview <?= $aktif_data?>">
                    <a href="#">
                        <i class="fa fa-bitbucket"></i>
                        <span>Data</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_dosen','index'))?>"><i class="fa fa-angle-double-right"></i>Data Dosen</a></li>
                        
                        
                        <li><a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_tahun','index'))?>"><i class="fa fa-angle-double-right"></i>Data Tahun Akademik</a></li>
                        
                        <li><a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_jenis_ujian','index'))?>"><i class="fa fa-angle-double-right"></i>Data Jenis Ujian</a></li>
                        
                        <!--<li><a href="pages/charts/inline.html"><i class="fa fa-angle-double-right"></i>Lamaran Karyawan</a></li>-->
                    </ul>
                </li>
<!--                <li class="treeview <?= $aktif_laporan?>">
                    <a href="#">
                        <i class="fa fa-home"></i>
                        <span>BPJS</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu ">
                        <li class="treeview ">
                            <a href="#">
                                <i class="fa fa-hospital-o"></i>
                                <span>BPJS Kesehatan</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?= site_url($this->mza_secureurl->setSecureUrl_encode('ctrl_bpjs_kesehatan','gotoAktifasiData'))?>"><i class="fa fa-angle-double-right"></i>Aktifasi Data</a></li>
                                <li><a href="<?= site_url($this->mza_secureurl->setSecureUrl_encode('ctrl_bpjs_kesehatan','gotoDataBPJS'))?>"><i class="fa fa-angle-double-right"></i>Data BPJS</a></li>
                                
                            </ul>
                            
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-briefcase   "></i>
                                <span>BPJS Ketenagakerjaan (JAMSOSTEK)</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?= site_url($this->mza_secureurl->setSecureUrl_encode('ctrl_bpjs_ketenagakerja','gotoAktifasiData'))?>"><i class="fa fa-angle-double-right"></i>Aktifasi Data</a></li>
                                <li><a href="<?= site_url($this->mza_secureurl->setSecureUrl_encode('ctrl_bpjs_ketenagakerja','gotoDataBPJS'))?>"><i class="fa fa-angle-double-right"></i>Data BPJS</a></li>
                            </ul>
                            
                            
                        </li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Lamaran Karyawan</a></li>
                    </ul>
                </li>-->
               
<!--                <li>
                    <a href="pages/mailbox.html">
                        <i class="fa fa-envelope"></i> <span>Mailbox</span>
                        <small class="badge pull-right bg-yellow">12</small>
                    </a>
                </li>-->
                
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>