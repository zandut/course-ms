<!-- jQuery 2.0.2 -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/jquery-1.11.1.min.js'); ?>"></script>
<!--<script src="<?= base_url('assets/js/jquery-ui-1.10.3.js'); ?>"></script>-->
<script src="<?= base_url('assets/js/jquery-ui.js'); ?>"></script>

<!-- Bootstrap -->
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<!-- Flot charts -->

<script src="<?= base_url('assets/js/plugins/flot/jquery.flot.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/js/plugins/flot/jquery.flot.resize.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/js/plugins/flot/jquery.flot.pie.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/js/plugins/flot/jquery.flot.categories.min.js'); ?>" type="text/javascript"></script>
<!-- Sparkline -->
<!-- daterangepicker -->
<!--<script src="<?= base_url('assets/js/plugins/daterangepicker/daterangepicker.js'); ?>" type="text/javascript"></script>-->
<!-- datepicker -->
<script src="<?= base_url('assets/js/plugins/datepicker/js/bootstrap-datepicker.js'); ?>" type="text/javascript"></script>
<!--timepicker-->
<script src="<?= base_url('assets/js/plugins/timepicker/jquery.timepicker.js'); ?>" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="<?= base_url('assets/js/AdminLTE/app.js'); ?>" type="text/javascript"></script>

<!-- AdminLTE for demo purposes -->
<!--<script src="<?= base_url('assets/js/AdminLTE/demo.js'); ?>" type="text/javascript"></script>-->

<script type="text/javascript" src="<?= base_url('assets/js/getChapter.js')?>"></script>

<script type="text/javascript" src="<?= base_url('assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/plugins/ckeditor/ckeditor.js') ?>"></script>
<!-- date tables -->
<script src="<?= base_url('assets/js/plugins/datatables/dataTables.tableTools.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/js/plugins/datatables/jquery.dataTables.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/js/plugins/datatables/dataTables.bootstrap.js') ?>" type="text/javascript"></script>

<script src="<?= base_url('assets/js/plugins/countdown/flipclock.js') ?>"></script>

<script>
    
    <?php 
        if ($tugas_terdekat != NULL)
        {
    
    ?>
        $(document).ready(function() {
                var clock;

                clock = $('.clockTugas').FlipClock({
                clockFace: 'DailyCounter',
                autoStart: false,
                callbacks: {
                        stop: function() {
                                $('.message').html('The clock has stopped!')
                        }
                }

            });

            <?php 

                date_default_timezone_set('Asia/Jakarta');
                $today1 = new DateTime();

                $test1 = new DateTime($tugas_terdekat->TGL_KUMPUL." ".$tugas_terdekat->JAM_KUMPUL);

                if ($test1 > $today1)
                {
                    $selisih = $today1->diff($test1);

                    $selisih1 = $selisih->d*3600*24 + $selisih->h*3600 + $selisih->i*60 + $selisih->s;  
                }
            ?>


            clock.setTime(<?= $selisih1 ?>);
            clock.setCountdown(true);
            clock.start();

        });
    <?php 
        }
        
        if ($kuis_terdekat != NULL)
        {
    ?>

        $(document).ready(function() {
                var clock;

                clock = $('.clockKuis').FlipClock({
                clockFace: 'DailyCounter',
                autoStart: false,
                callbacks: {
                        stop: function() {
                                $('.message').html('The clock has stopped!')
                        }
                }

            });

            <?php 
                date_default_timezone_set('Asia/Jakarta');
                $today = new DateTime();

                $test = new DateTime($kuis_terdekat->TGL." ".$kuis_terdekat->JAM);
                if ($test > $today)
                {
                    $selisih = $today->diff($test);


                    $selisih1 = $selisih->d*3600*24 + $selisih->h*3600 + $selisih->i*60 + $selisih->s;
                }
            ?>


            clock.setTime(<?= $selisih1 ?>);
            clock.setCountdown(true);
            clock.start();

        }); 
    <?php 
        }
    ?>

</script>
<script type="text/javascript">
    
    

    $(function() {
        $('.tgl_lahir').datepicker({
            endDate: '31-12-1996',
            startDate: '31-12-1966'

        });
        
        

        $('.tgl').datepicker({
            startDate: '<?= date('M-DD-YYYY')?>'
        }
                
        );

        $('.timepicker').timepicker({
            timeFormat: 'H:i',
            step: 15

        });



        var table = $("#test").DataTable();

        //        var table = $('#test').DataTable();
        //        var tt = new $.fn.dataTable.TableTools(table);
        //
        //        $(tt.fnContainer()).insertBefore('div.dataTables_wrapper');




    });

</script>
<script>
    $(function() {
        CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor

    });
</script>


<!--    <script type="text/javascript">
        $(function() {

            /* END BAR CHART */

            /*
             * LINE CHART
             * ----------
             */
            //LINE randomly generated data


            var sin = [], cos = [];
<?php
$i = 1;
foreach ($data_chart as $data) {
    ?>
                    sin.push([<?= $i ?>, <?= $data ?>]);

    <?php
    $i++;
}
?>
            var line_data1 = {
                data: sin,
                color: "#3c8dbc"
            };

            $.plot("#line-chart", [line_data1], {
                grid: {
                    hoverable: true,
                    borderColor: "#f3f3f3",
                    borderWidth: 1,
                    tickColor: "#f3f3f3"
                },
                series: {
                    shadowSize: 0,
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                lines: {
                    fill: false,
                    color: ["#3c8dbc", "#f56954"]
                },
                yaxis: {
                    show: true,
                },
                xaxis: {
                    show: true
                }
            });
            //Initialize tooltip on hover
            $("<div class='tooltip-inner' id='line-chart-tooltip'></div>").css({
                position: "absolute",
                display: "none",
                opacity: 0.8
            }).appendTo("body");
            $("#line-chart").bind("plothover", function(event, pos, item) {

                if (item) {
                    var x = item.datapoint[0].toFixed(2),
                            y = item.datapoint[1].toFixed(2);

                    $("#line-chart-tooltip").html(item.series.label + " of " + x + " = " + y)
                            .css({top: item.pageY + 5, left: item.pageX + 5})
                            .fadeIn(200);
                } else {
                    $("#line-chart-tooltip").hide();
                }

            });
            /* END LINE CHART */
        });
    </script>-->
</body>
</html>