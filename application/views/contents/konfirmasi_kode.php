<div class="form-box" id="login-box">
    <?php
    if (isset($error) && $error == TRUE) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-warning"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>PERHATIAN !!</b><br> Kode salah. <?= $jml." kali kesempatan !!" ?>
        </div>
        <?php
    } 
?>

    <div class="header"><?= $header_title ?>


    </div>


<?= form_open($this->mza_secureurl->setsecureurl_encode('ctrl_mahasiswa','konfirmasi_kode')); ?>
    <div class="body bg-gray">
        <div class="form-group">
            <label>Masukkan Kode Keamanan</label>
            <input type="password" name="kode" class="form-control" placeholder="Kode Keamanan"/>
        </div>
        
    </div>
    <div class="footer">   

        <button type="submit" class="btn bg-olive btn-block">Masuk</button>  
        <a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_mahasiswa','logOut')) ?>" class="text-center">Kembail ke login</a>
        
    </div>
</form>


    <!--    <div class="margin text-center">
            <span>Sign in using social networks</span>
            <br/>
            <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
            <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
            <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>
    
        </div>-->
</div>



