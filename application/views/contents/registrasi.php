
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Perkuliahan
            <small>Registrasi</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Perkuliahan</a></li>
            <li class="active">Registrasi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <?php 
                    if ($success == TRUE)
                    {
                ?>
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>SUCCESS !!</b>
                        </div>
                <?php 
                    }
                    else
                        if ($error == TRUE)
                        {
                           
                ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-warning"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>PERHATIAN !!</b><br><?= validation_errors()?>
                            </div>
                <?php 
                        }
                ?>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Tambah Mata Kuliah</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body ">
                        <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_registrasi','insertMatkul', array($nama))) ?>
                        <div class="form-group">
                            <label>Masukkan Kode Matkul</label>
                            <input class="form-control" type="text" name="kode_matkul" placeholder="KODE MATKUL"/>
                        </div>
                        <div class="form-group">
                            <label>Pilih Dosen</label>
                            <select name="kode_dosen" class="form-control">
                                <option>---Pilih Dosen---</option>
                                <?php 
                                    foreach ($data_dosen as $data) {

                                    
                                ?>
                                
                                <option value="<?= $data->KODE_DOSEN ?>"><?= $data->NAMA_DOSEN ?></option>
                                
                                <?php 
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Masukkan Nama Matkul</label>
                            <input class="form-control" type="text" name="nama_matkul" placeholder="NAMA MATKUL"/>
                        </div>
                        <div class="footer">
                            <button class="btn btn-success" type="submit"><li class="fa fa-check"> Simpan</li></button>
                            
                        </div>
                        </form>
                        

                    </div>
                </div><!-- /.box -->
            </div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Mata Kuliah</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body ">
                        <div class="box-body table-responsive">
                            <script>
                                function getId(id)
                                {
                                    document.getElementById('kode').value = id;
                                }
                            </script>
                            <table id="test" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="col-xs-2">Kode Matkul</th>
                                        <th class="col-xs-3">Nama Matkul (Jumlah Chapter)</th>
                                        <th class="col-xs-2">Nama Dosen</th>
                                        <th class="col-xs-1">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach ($data_matkul as $data) {


    //                                ?>
                                            <tr>
                                                <td class=""><?= $data->KODE_MATKUL ?></td>
                                                <td class=""><?= $data->NAMA_MATKUL." (".$jumlah_chapter[$data->KODE_MATKUL].")" ?></td>
                                                <td class=""><?= $data->NAMA_DOSEN ?></td>
                                                
                                                <td>
                                                    <a href="<?= site_url($this->mza_secureurl->setSecureUrl_encode('ctrl_registrasi','detail_matkul',array($data->KODE_MATKUL))); ?>" ><li class="fa fa-search" data-toggle="tooltip" title="Detail"  ></li></a>
                                                    <a href="#" data-toggle="modal" data-target="#konfirmasi" onclick="getId('<?= $data->KODE_MATKUL ?>')"><li class="fa fa-trash-o" data-toggle="tooltip" title="Hapus"  ></li></a>
                                                </td>
                                            </tr>
                                   <?php 
                                        }
                                   ?>
                                </tbody>
        <!--                        <tfoot>
                                    <tr>
                                        <th>Rendering engine</th>
                                        <th>Browser</th>
                                        <th>Platform(s)</th>
                                        <th>Engine version</th>
                                        <th>CSS grade</th>
                                        <th>Rendering engine</th>
                                        <th>Browser</th>
                                        <th>Platform(s)</th>
                                        <th>Engine version</th>
                                        <th>CSS grade</th>
                                    </tr>
                                </tfoot>-->
                            </table>
                        </div>
                        <div class="modal fade" id="konfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-blue-gradient">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                    </div>

                                    <div class="modal-body bg-gray">
                                        <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_registrasi','deleteMatkul')) ?>
                                        <div class="form-group">
                                            <label>YAKIN MENGHAPUS ? </label>
                                            <input class="hidden" name="kode" id="kode"/>
                                            <br>
                                            <button type="submit" class="btn btn-success" ><li class="fa fa-check"></li> Hapus</button>

                                        </div>

                                        </form>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                    </div>

                    </div>
            </div>
                
        </div>
            
        
    </section><!-- /.content -->

    <!--History Jabatan-->


</aside><!-- /.right-side -->

</div>
