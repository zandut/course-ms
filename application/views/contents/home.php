<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Home
            <small>Dashboard</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    
    

    <!-- Main content -->
    <section class="content">

        <!-- Small boxes (Stat box) -->
        
        <div class="row">
            <?php 
                if ($tugas_terdekat != null)
                {
            ?>
            <section class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Tugas Terdekat</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-primary btn-xs" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <!--<button class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div>
                            <h1>MATA KULIAH : <?= $tugas_terdekat->NAMA_MATKUL ?></h1>
                            <h1>JUDUL TUGAS : <a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_tugas','index'))?>"><?= $tugas_terdekat->JUDUL_TUGAS ?></a></h1>
                            <h1>DI KUMPUL : <?= date_format(date_create($tugas_terdekat->TGL_KUMPUL), 'd/m/Y')." ".$tugas_terdekat->JAM_KUMPUL?></h1>
                        </div>
                        <br>
                        <div class="clockTugas"></div>
                    </div>
                </div>
                
                
            </section>
            <?php 
                }
                
                if ($kuis_terdekat != NULL)
                {
            ?>
            <section class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Kuis Terdekat</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-primary btn-xs" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <!--<button class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div>
                            <h1>MATA KULIAH : <?= $kuis_terdekat->NAMA_MATKUL ?></h1>
                            <h1>CHAPTER : <a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_kuis','index'))?>"><?= $kuis_terdekat->NAMA_CHAPTER ?></a></h1>
                            <h1>DI KUMPUL : <?= date_format(date_create($kuis_terdekat->TGL), 'd/m/Y')." ".$kuis_terdekat->JAM?></h1>
                        </div>
                        <br>
                        <div class="clockKuis"></div>
                    </div>
                </div>
                
                
            </section>
            <?php 
                }
            ?>
            
            
            
        </div> 

    </section><!-- /.content -->
</aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<!-- add new calendar event modal -->