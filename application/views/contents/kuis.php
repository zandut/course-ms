
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Perkuliahan
            <small>Kuis</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Perkuliahan</a></li>
            <li class="active">Kuis</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class=" col-md-4">
                <?php 
                    if ($success == TRUE)
                    {
                ?>
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>SUCCESS !!</b>
                        </div>
                <?php 
                    }
                    else
                        if ($error == TRUE)
                        {
                           
                ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-warning"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>PERHATIAN !!</b><br><?= validation_errors()?>
                            </div>
                <?php 
                        }
                ?>
                <script >
                    function getId(id)
                    {
                        document.getElementById("id_kuis").value = id;
                    }
                    
                    function getData(id, judul, tgl, jam, nilai, id_chapter, keterangan)
                    {
                        document.getElementById("nama_kuis").value = judul;
                        document.getElementById("tgl_kuis").value = tgl;
                        document.getElementById("jam_kuis").value = jam;
                        document.getElementById("nilai").value = nilai;
                        document.getElementById("id_chapter").value = id_chapter;
                        document.getElementById("keterangan").value = keterangan;
                        
                        document.getElementById("id").value = id;
                    }

                </script>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Tambah Kuis</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body ">

                        <?= form_open($this->mza_secureurl->setSecureurl_encode('ctrl_kuis','insertKuis',array())) ?>
                        <div class="form-group">
                            <label>Pilih Mata Kuliah</label>
                            <select class="form-control" name="kode_matkul" onchange="showChapter(this.value)" >
                                <option value="" >---Pilih Matkul---</option>
                                <?php 
                                    foreach ($data_matkul as $data) {
                                                
                                
                                ?>
                                <option value="<?= $data->KODE_MATKUL?>"><?= $data->NAMA_MATKUL." (".$data->NAMA_DOSEN.")"?></option>
                                <?php 
                                    }
                                ?>
                                
                            </select>
                                
                        </div>
                        <div class="form-group">
                            <label>Pilih Chapter</label>
                            
                            <select id="chapter" name="id_chapter" class="form-control" >
                                <option value="">---Pilih Chapter---</option>
                            </select>
                                
                        </div>
                        
                        <div class="form-group">
                            <label>Pilih Tanggal Kuis</label>
                            
                            <input class="form-control tgl" type="text" name="tgl" placeholder="TANGGAL KUIS"/> 
                                
                        </div>
                        <div class="form-group">
                            <label>Pilih Jam Kuis</label>
                            
                            <input class="form-control timepicker" type="text" name="jam" placeholder="JAM KUIS"/> 
                                
                        </div>
                        <div class="form-group">
                            <label>Masukkan Keterangan</label>
                            <input class="form-control" type="text" name="keterangan" placeholder="KETERANGAN" />
                        </div>
                        
                        <div class="form-group footer">
                            <button class="btn btn-success" type="submit"><li class="fa fa-check"></li> Simpan</button>
                        </div>
                        </form>
                       
                    </div><!-- /.box-header -->

                  

                </div><!-- /.box -->
            </div>
            <div class=" col-md-8">
               
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Kuis</h3>
                    </div><!-- /.box-header -->
                    
                    <div class="box-body table-responsive">
                        <table id="test" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="col-xs-2">Nama Kuis</th>
                                    <th class="col-xs-2">Chapter</th>
                                    <th class="col-xs-2">Mulai</th>
                                   <th class="col-xs-2">Keterangan</th>
                                    <th class="col-xs-1">Nilai</th>
                                    
                                    <th class="col-xs-1">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($data_kuis as $data) {
                                        
                                    
//                                ?>
                                        <tr>
                                            <td class=""><?= $data->NAMA_KUIS ?></td>
                                            <td class=""><?= $data->NAMA_CHAPTER ?></td>
                                            
                                            <td class=""><?= date_format(date_create($data->TGL), "d/m/Y")." ".$data->JAM ?></td>
                                            <td ><?= $data->KETERANGAN?></td>
                                             <td ><?= $data->NILAI?></td>
                                            
                                            <td>
                                                
                                                <a href="#" data-toggle="modal" data-target="#editDosen" onclick="getData('<?= $data->ID_KUIS ?>','<?= $data->NAMA_KUIS ?>','<?= $data->TGL ?>','<?= $data->JAM ?>','<?= $data->NILAI ?>','<?= $data->ID_CHAPTER ?>','<?= $data->KETERANGAN ?>')"><li class="fa fa-pencil-square-o" data-toggle="tooltip" title="Edit"  ></li></a>
                                                <a href="#" data-toggle="modal" data-target="#konfirmasi" onclick="getId('<?= $data->ID_KUIS ?>')"><li class="fa fa-trash-o" data-toggle="tooltip" title="Hapus"  ></li></a>
                                            </td>
                                        </tr>
                               <?php 
                                    }
                               ?>
                            </tbody>
    <!--                        <tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                </tr>
                            </tfoot>-->
                        </table>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box -->
                
            </div>
            
            <div class="modal fade" id="konfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-blue-gradient">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                        </div>
                        
                        <div class="modal-body bg-gray">
                            <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_kuis','deleteKuis',array())) ?>
                            <div class="form-group">
                                <label>YAKIN MENGHAPUS ? </label>
                                <input class="hidden" name="id_kuis" id="id_kuis"/>
                                <br>
                                <button type="submit" class="btn btn-success" ><li class="fa fa-check"></li> Hapus</button>

                            </div>

                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="editDosen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-blue-gradient">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Edit Data Kuis</h4>
                        </div>
                        <div class="modal-body bg-gray">
                            <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_kuis','updateKuis',array())) ?>
                            <div hidden="">
                                <input name="id_kuis" id="id" />
                                <input name="id_chapter" id="id_chapter" />
                            </div>
                            <div class="form-group">
                                <label>Nama Kuis</label>

                                <input class="form-control" type="text" id="nama_kuis" name="nama_kuis" readonly placeholder="NAMA KUIS"/> 

                            </div>
                            <div class="form-group">
                                <label>Tanggal Kuis</label>

                                <input class="form-control tgl" type="text" id="tgl_kuis" name="tgl" placeholder="TANGGAL KUIS"/> 

                            </div>
                            <div class="form-group">
                                <label>Jam Kuis</label>

                                <input class="form-control timepicker" type="text" id="jam_kuis" name="jam" placeholder="JAM KUIS"/> 

                            </div>
                            <div class="form-group">
                                <label>Nilai Kuis</label>

                                <input class="form-control" type="text" id="nilai" name="nilai" placeholder="NILAI"/> 

                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>

                                <input class="form-control" type="text" id="keterangan" name="keterangan" placeholder="KETERANGAN"/> 

                            </div>
                            
                            <div class="form-group">
                                
                                <button type="submit" class="btn btn-success" ><li class="fa fa-check"></li> Simpan</button>
                            </div>
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section><!-- /.content -->

    <!--History Jabatan-->


</aside><!-- /.right-side -->

</div>
