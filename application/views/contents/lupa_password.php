<div class="form-box" id="login-box">
    <?php
    if (isset($error) && $error == TRUE) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-warning"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>PERHATIAN !!</b><br> Username tidak tersedia
        </div>
        <?php
    } 
?>

    <div class="header"><?= $header_title ?>


    </div>


<?= form_open($this->mza_secureurl->setsecureurl_encode('ctrl_lupa_password','checkUsername')); ?>
    <div class="body bg-gray">
        <div class="form-group">
            <label>Masukkan Username</label>
            <input type="text" name="userid" class="form-control" placeholder="Username"/>
        </div>
        
    </div>
    <div class="footer">   

        <button type="submit" class="btn bg-olive btn-block">Reset Password</button>  
        <a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('welcome','index')) ?>" class="text-center">Kembail ke login</a>
        
    </div>


    <!--    <div class="margin text-center">
            <span>Sign in using social networks</span>
            <br/>
            <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
            <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
            <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>
    
        </div>-->
</div>



