<div class="form-box" id="login-box">
    <?php 
    if (isset($error) && $error == TRUE)
    {
    ?>
        <div class="alert alert-warning alert-dismissable">
            <i class="fa fa-warning"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>PERHATIAN !!</b><br> <?= validation_errors()?>
        </div>
    <?php 
    }
    else
        if (isset ($success) && $success == true)
        {
            
    ?>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b></b>Password berhasil di reset
            </div>
    <?php
        }
        
            
    ?>
    <div class="header"><?= $header_title ?>
        
    
    </div>
    
    
    <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_lupa_password','resetPassword', array($username))); ?>
        <div class="body bg-gray">
            
            <div class="form-group">
                <label>Password Baru</label>
                <input type="password" name="password" class="form-control" placeholder="Password"/>
            </div>         
            <div class="form-group">
                <label>Ketik Ulang</label>
                <input type="password" name="password1" class="form-control" placeholder="Konfirmasi Password"/>
            </div>
            
        </div>
        <div class="footer">   
            
            <button type="submit" class="btn bg-olive btn-block">Simpan</button>  

            <a href="<?=  site_url($this->mza_secureurl->setsecureurl_encode('welcome','index'))?>" class="text-center">Kembali ke login</a>
        </div>
    </form>

<!--    <div class="margin text-center">
        <span>Sign in using social networks</span>
        <br/>
        <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
        <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
        <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>

    </div>-->
</div>



