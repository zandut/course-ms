<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Perkuliahan
            <small>Mulai Kuliah</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Perkuliahan</a></li>
            <li class="active">Mulai Kuliah</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php 
                    if ($success == TRUE)
                    {
                ?>
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>SUCCESS !!</b>
                        </div>
                <?php 
                    }
                    else
                        if ($error == TRUE)
                        {
                           
                ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-warning"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>PERHATIAN !!</b><br><?= validation_errors()?>
                            </div>
                <?php 
                        }
                ?>
                
                <script>
                    
                    
                    function showCatatan(str) {
                
//                        document.getElementById("grup").innerHTML = "<select class='form-control' name='id_chapter'></select><option>Pilih Chapter</option>";
//                        document.getElementById("chapter").innerHTML = "<select class='form-control' name='id_chapter'></select><option>---Pilih Chapter</option>";
                        
                        if (str == "") {
                            document.getElementById("editor1").value = "";
                                
                            return;
                        }
                        if (window.XMLHttpRequest) {
                            // code for IE7+, Firefox, Chrome, Opera, Safari
                            xmlhttp = new XMLHttpRequest();
                        } else { // code for IE6, IE5
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xmlhttp.onreadystatechange = function() {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                document.getElementById("editor1").value = xmlhttp.responseText;

                            }
                        }
                        var base = window.location.origin;
//                        alert(str);

                        xmlhttp.open("GET", base + "/course-ms/index.php/ctrl_mulai_kuliah/getCatatan?id=" + str, true);
                        xmlhttp.send();
                        
                        
                    }
                
                </script>
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Catatan Mata Kuliah</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body ">
                        <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_mulai_kuliah','gotoDetail', array())) ?>
                        <div class="form-group">
                            <label>Pilih Mata Kuliah</label>
                            <select name="kode_matkul" class="form-control" onchange="showChapter(this.value)">
                                <option value="">---Pilih Mata Kuliah---</option>
                                <?php 
                                    foreach ($data_kuliah as $data) {

                                ?>
                                
                                <option value='<?= $data->KODE_MATKUL ?>'><?= $data->NAMA_MATKUL." (".$data->NAMA_DOSEN.")" ?></option>
                                
                                <?php 
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Pilih Chapter</label>
                            <select id="chapter" class="form-control" name="id_chapter" onchange="showCatatan(this.value)">
                                <option value="">---Pilih Chapter---</option>
                            </select>
                            
                        </div>
                        
<!--                        <div class="form-group">
                            <label>Catatan</label>

                            <textarea id="editor1" name="catatan" rows="10" cols="80">
                                <?php 
                                    foreach ($data_kuliah as $data) {
                                        echo $data->CATATAN;    
                                    }
                                ?>
                            </textarea>

                        </div>-->
                        
                        <div class="footer">
                            <button class="btn btn-success" type="submit"><li class="fa fa-arrow-circle-right"> Mulai</li></button>
                            <a href="#" data-toggle="modal" data-target="#tambah" class="btn btn-primary" ><li class="fa fa-plus-circle"> Tambah Chapter</li></a>
                        </div>
                        </form>
                        

                    </div>
                </div><!-- /.box -->
            </div>
            <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-blue-gradient">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Tambah Chapter</h4>
                        </div>

                        <div class="modal-body bg-gray">
                            <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_mulai_kuliah','insertChapter')) ?>
                            <div class="form-group">
                                <label>Pilih Mata Kuliah</label>
                                <select class="form-control" name="kode_matkul" >
                                    <option value="">---Pilih Mata Kuliah---</option>
                                    <?php 
                                        foreach ($data_kuliah as $data) {

                                    ?>
                                    <option value="<?= $data->KODE_MATKUL?>" ><?= $data->NAMA_MATKUL." (Bp. ".$data->NAMA_DOSEN.")"?></option>
                                    <?php
                                        }   
                                    ?>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>Nama Chapter</label>
                                <input class="form-control" name="nama_chapter" placeholder="NAMA CHAPTER"/>
                                
                            </div>
                            
                            
                            <div class="footer ">
                                <button type="submit" class="btn btn-success " ><li class="fa fa-check"></li> Simpan</button>
                                <button type="reset" class="btn btn-warning"><li class="fa fa-refresh"></li>  Reset</button>
                            </div>

                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>  
            
        
    </section><!-- /.content -->

    <!--History Jabatan-->


</aside><!-- /.right-side -->

</div>
