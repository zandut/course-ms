<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Registrasi
            <small>Detail Kuliah</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Perkuliahan</a></li>
            <li><a href="#">Registrasi</a></li>
            <li class="active">Detail Kuliah</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <?php 
                    if ($success == TRUE)
                    {
                ?>
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>SUCCESS !!</b>
                        </div>
                <?php 
                    }
                    else
                        if ($error == TRUE)
                        {
                           
                ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-warning"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>PERHATIAN !!</b><br><?= validation_errors()?>
                            </div>
                <?php 
                        }
                ?>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Detail Mata Kuliah</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body ">
                    <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_registrasi','updateMatkul')) ?>
                        <div class="form-group">
                            
                            <label>Kode Matkul</label>
                            <input class="form-control" name="kode_matkul" readonly="" value="<?= $data_matkul->KODE_MATKUL ?>" />
                        </div>
                        <div class="form-group">
                            <label>Nama Matkul</label>
                            <input class="form-control" name="nama_matkul" value="<?= $data_matkul->NAMA_MATKUL ?>" />
                        </div>
                        <div class="form-group">
                            <label>Dosen Pengajar</label>
                            <select class="form-control" name="kode_dosen" >
                                <option value="">---Pilih Dosen---</option>
                                <?php 
                                    $selected = "";
                                    foreach ($data_dosen as $data) {
                                        if ($data->KODE_DOSEN == $data_matkul->KODE_DOSEN)
                                        {
                                            $selected = "selected";
                                        }
                                        else
                                        {
                                            $selected = "";
                                        }
                                    
                                
                                ?>
                                    <option value="<?= $data->KODE_DOSEN ?>" <?= $selected ?>><?= $data->NAMA_DOSEN ?></option>
                                <?php 
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="footer">
                            <button class="btn btn-success" type="submit"><li class="fa fa-check"> Simpan</li></button>
                        </div>
                    </form>
                        
                    </div>
                </div><!-- /.box -->
            </div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Chapter</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body" >
                        <a href="#" data-toggle="modal" data-target="#tambah" class="btn btn-primary" ><li class="fa fa-plus-circle"> Tambah Chapter</li></a>

                    </div>
                    <div class="box-body ">
                        
                        <div class="box-body table-responsive">
                            <script>
                                function getId(id,kode)
                                {
                                    document.getElementById('id').value = id;
                                    document.getElementById('kode').value = kode;
                                }
                                
                                function getChapter(id,nama)
                                {
                                    document.getElementById('id_chapter').value = id;
                                    document.getElementById('nama').value = nama;
                                }
                            </script>
                            <table id="test" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="col-xs-1">ID Chapter</th>
                                        <th class="col-xs-2">Nama Chapter</th>
                                        
                                        <th class="col-xs-1">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach ($data_chapter as $data) {


    //                                ?>
                                            <tr>
                                                <td class=""><?= $data->ID_CHAPTER ?></td>
                                                <td class=""><?= $data->NAMA_CHAPTER ?></td>
                                                
                                                
                                                <td>
                                                    
                                                    <a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('ctrl_mulai_kuliah','gotoDetailByIdChapter',array($data->ID_CHAPTER))) ?>" ><li class="fa fa-briefcase" data-toggle="tooltip" title="Belajar"  ></li></a>
                                                    <a href="#" data-toggle="modal" data-target="#editChapter" onclick="getChapter('<?= $data->ID_CHAPTER ?>','<?= $data->NAMA_CHAPTER ?>')"><li class="fa fa-pencil-square-o" data-toggle="tooltip" title="Edit"  ></li></a>
                                                    <a href="#" data-toggle="modal" data-target="#konfirmasi" onclick="getId('<?= $data->ID_CHAPTER ?>','<?= $data->KODE_MATKUL ?>')"><li class="fa fa-trash-o" data-toggle="tooltip" title="Hapus"  ></li></a>
                                                </td>
                                            </tr>
                                   <?php 
                                        }
                                   ?>
                                </tbody>
        <!--                        <tfoot>
                                    <tr>
                                        <th>Rendering engine</th>
                                        <th>Browser</th>
                                        <th>Platform(s)</th>
                                        <th>Engine version</th>
                                        <th>CSS grade</th>
                                        <th>Rendering engine</th>
                                        <th>Browser</th>
                                        <th>Platform(s)</th>
                                        <th>Engine version</th>
                                        <th>CSS grade</th>
                                    </tr>
                                </tfoot>-->
                            </table>
                        </div>
                        <div class="modal fade" id="konfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-blue-gradient">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                    </div>

                                    <div class="modal-body bg-gray">
                                        <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_registrasi','deleteChapter')) ?>
                                        <div class="form-group">
                                            <label>YAKIN MENGHAPUS ? </label>
                                            <input class="hidden" name="id" id="id"/>
                                            <input class="hidden" name="kode" id="kode"/>
                                            <br>
                                            <button type="submit" class="btn btn-success" ><li class="fa fa-check"></li> Hapus</button>

                                        </div>

                                        </form>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-blue-gradient">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Tambah Chapter</h4>
                                </div>

                                <div class="modal-body bg-gray">
                                    <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_registrasi','insertChapter')) ?>
                                    <div class="form-group">
                                        <label>Kode Matkul</label>
                                        <input class="form-control" name="kode_matkul" readonly="" value="<?= $data_matkul->KODE_MATKUL?>" />
                                        
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Matkul</label>
                                        <input class="form-control" value="<?= $data_matkul->NAMA_MATKUL?>" readonly="" />
                                    </div>

                                    <div class="form-group">
                                        <label>Nama Chapter</label>
                                        <input class="form-control" name="nama_chapter" placeholder="NAMA CHAPTER"/>

                                    </div>


                                    <div class="footer ">
                                        <button type="submit" class="btn btn-success " ><li class="fa fa-check"></li> Simpan</button>
                                        <button type="reset" class="btn btn-warning"><li class="fa fa-refresh"></li>  Reset</button>
                                    </div>

                                    </form>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="editChapter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-blue-gradient">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Edit Chapter</h4>
                                </div>

                                <div class="modal-body bg-gray">
                                    <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_registrasi','editChapter')) ?>
                                    <div class="form-group">
                                        <label>ID Chapter</label>
                                        <input class="form-control" name="id_chapter" readonly="" id="id_chapter"/>
                                        
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Nama Chapter</label>
                                        <input class="form-control" name="nama_chapter" placeholder="NAMA CHAPTER" id="nama"/>

                                    </div>


                                    <div class="footer ">
                                        <button type="submit" class="btn btn-success " ><li class="fa fa-check"></li> Simpan</button>
                                        
                                    </div>

                                    </form>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    </div>
            </div>
                
        </div>
            
        
    </section><!-- /.content -->

    <!--History Jabatan-->


</aside><!-- /.right-side -->

</div>
