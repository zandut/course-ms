
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data
            <small>Data Dosen</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Data</a></li>
            <li class="active">Data Dosen</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class=" col-md-4">
                <?php 
                    if ($success == TRUE)
                    {
                ?>
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>SUCCESS !!</b>
                        </div>
                <?php 
                    }
                    else
                        if ($error == TRUE)
                        {
                           
                ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-warning"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>PERHATIAN !!</b><br><?= validation_errors()?>
                            </div>
                <?php 
                        }
                ?>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Tambah Dosen</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body ">

                        <?= form_open($this->mza_secureurl->setSecureurl_encode('ctrl_dosen','insertDosen',array())) ?>
                        <div class="form-group">
                            <label>Masukkan Kode Dosen</label>
                            
                            <input class="form-control" type="text" name="kode_dosen" placeholder="Kode Dosen"/> 
                                
                        </div>
                        <div class="form-group">
                            <label>Masukkan Nama Dosen</label>
                            
                            <input class="form-control" type="text" name="nama_dosen" placeholder="Nama Dosen"/> 
                                
                        </div>
                        <div class="form-group">
                            <label>Masukkan E-mail Dosen</label>
                            
                            <input class="form-control" type="text" name="email" placeholder="E-mail Dosen"/> 
                                
                        </div>
                        <div class="form-group">
                            <label>Masukkan No Hp Dosen</label>
                            
                            <input class="form-control" type="text" name="hp" placeholder="No HP Dosen"/> 
                                
                        </div>
                        
                        <div class="form-group footer">
                            <button class="btn btn-success" type="submit"><li class="fa fa-check"></li> Simpan</button>
                        </div>
                        </form>
                       
                    </div><!-- /.box-header -->

                  

                </div><!-- /.box -->
            </div>
            <div class=" col-md-8">
               
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Dosen</h3>
                    </div><!-- /.box-header -->
                    <script >
                        function getId(id)
                        {
                            document.getElementById("kode_dosen").value = id;
                        }
                        
                        function getDataDosen(id)
                        {
                            if (window.XMLHttpRequest) {
                                // code for IE7+, Firefox, Chrome, Opera, Safari
                                xmlhttp = new XMLHttpRequest();
                            } else { // code for IE6, IE5
                                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                            }
                            xmlhttp.onreadystatechange = function() {
                                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                    document.getElementById("data_dosen").innerHTML = xmlhttp.responseText;
                                }
                            }
                            var base = window.location.origin;
//                            alert(base + "/aplikasihrd/index.php/ctrl_bagian/getBagian?id=" + id);

                            xmlhttp.open("GET", base + "/course-ms/index.php/ctrl_dosen/getDosen?id="+ id, true);
                            xmlhttp.send();
                        }
                    
                    </script>
                    <div class="box-body table-responsive">
                        <table id="test" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="col-xs-2">Kode Dosen</th>
                                    <th class="col-xs-2">Nama Dosen</th>
                                    <th class="col-xs-2">E-mail Dosen</th>
                                    <th class="col-xs-2">No HP Dosen</th>
                                    <th class="col-xs-1">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($data_dosen as $data) {
                                        
                                    
//                                ?>
                                        <tr>
                                            <td class=""><?= $data->KODE_DOSEN ?></td>
                                            <td class=""><?= $data->NAMA_DOSEN ?></td>
                                            <td class=""><?= $data->EMAIL ?></td>
                                            <td class=""><?= $data->NO_HP ?></td>
                                            <td>
                                                <a href="#" ><li class="fa fa-mail-forward" data-toggle="tooltip" title="Kirim E-mail"></li></a>
                                                <a href="#" data-toggle="modal" data-target="#editDosen" onclick="getDataDosen('<?= $data->KODE_DOSEN ?>')"><li class="fa fa-pencil-square-o" data-toggle="tooltip" title="Edit"  ></li></a>
                                                <a href="#" data-toggle="modal" data-target="#konfirmasi" onclick="getId('<?= $data->KODE_DOSEN ?>')"><li class="fa fa-trash-o" data-toggle="tooltip" title="Hapus"  ></li></a>
                                            </td>
                                        </tr>
                               <?php 
                                    }
                               ?>
                            </tbody>
    <!--                        <tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                </tr>
                            </tfoot>-->
                        </table>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box -->
                
            </div>
            
            <div class="modal fade" id="konfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-blue-gradient">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                        </div>
                        
                        <div class="modal-body bg-gray">
                            <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_dosen','deleteDosen',array())) ?>
                            <div class="form-group">
                                <label>YAKIN MENGHAPUS ? </label>
                                <input class="hidden" name="kode_dosen" id="kode_dosen"/>
                                <br>
                                <button type="submit" class="btn btn-success" ><li class="fa fa-check"></li> Hapus</button>

                            </div>

                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="editDosen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-blue-gradient">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Edit Data Dosen</h4>
                        </div>
                        <div class="modal-body bg-gray">
                            <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_dosen','updateDosen',array())) ?>
                            <div id="data_dosen"></div>

                            <div class="form-group">
                                
                                <button type="submit" class="btn btn-success" ><li class="fa fa-check"></li> Simpan</button>
                            </div>
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section><!-- /.content -->

    <!--History Jabatan-->


</aside><!-- /.right-side -->

</div>
