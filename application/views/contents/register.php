<div class="form-box" id="login-box">
    <?php 
    if (isset($error) && $error == TRUE)
    {
    ?>
        <div class="alert alert-warning alert-dismissable">
            <i class="fa fa-warning"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>PERHATIAN !!</b> <?= validation_errors()?>
        </div>
    <?php 
    }
    else
        if (isset ($success) && $success == true)
        {
            
    ?>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b></b>Pendaftaran berhasil
            </div>
    <?php
        }
        
            
    ?>
    <div class="header"><?= $header_title ?>
        
    
    </div>
    
    
    <?= form_open($this->mza_secureurl->setsecureurl_encode('ctrl_register','SignUp')); ?>
        <div class="body bg-gray">
            <div class="form-group">
                
                <input type="text" name="userid" class="form-control" placeholder="NIM Anda"/>
            </div>
            <div class="form-group">
                <input type="text" name="nama" class="form-control" placeholder="Nama Anda"/>
            </div>
            <div class="form-group">
                <input type="text" name="alamat" class="form-control" placeholder="Alamat Anda"/>
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password"/>
            </div>         
            <div class="form-group">
                <input type="password" name="password1" class="form-control" placeholder="Konfirmasi Password"/>
            </div>
            
        </div>
        <div class="footer">   
            
            <button type="submit" class="btn bg-olive btn-block">Daftar</button>  

            <a href="<?= site_url($this->mza_secureurl->setsecureurl_encode('welcome','index'))?>" class="text-center">Sudah Memiliki Akun ?</a>
        </div>
    

<!--    <div class="margin text-center">
        <span>Sign in using social networks</span>
        <br/>
        <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
        <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
        <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>

    </div>-->
</div>



