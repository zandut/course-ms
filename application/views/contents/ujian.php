
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Perkuliahan
            <small>Ujian Mata Kuliah</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Perkuliahan</a></li>
            <li class="active">Ujian Mata Kuliah</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class=" col-md-4">
                <?php 
                    if ($success == TRUE)
                    {
                ?>
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>SUCCESS !!</b>
                        </div>
                <?php 
                    }
                    else
                        if ($error == TRUE)
                        {
                           
                ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-warning"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>PERHATIAN !!</b><br><?= validation_errors()?>
                            </div>
                <?php 
                        }
                ?>
                <script >
                    function getId(id)
                    {
                        document.getElementById("id_ujian").value = id;
                    }
                    
                    function getData(id, matkul, jenis, tgl, jam, keterangan, nilai)
                    {
                        document.getElementById("matkul").value = matkul;
                        document.getElementById("jenis_ujian").value = jenis;
                        
                        document.getElementById("tgl").value = tgl;
                        document.getElementById("jam").value = jam;
                        
                        document.getElementById("keterangan").value = keterangan;
                        document.getElementById("nilai").value = nilai;
                        document.getElementById("id").value = id;
                    }

                </script>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Tambah Ujian</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body ">

                        <?= form_open($this->mza_secureurl->setSecureurl_encode('ctrl_ujian','insertUjian',array())) ?>
                        <div class="form-group">
                            <label>Pilih Mata Kuliah</label>
                            <select class="form-control" name="kode_matkul" >
                                <option value="" >---Pilih Matkul---</option>
                                <?php 
                                    foreach ($data_matkul as $data) {
                                                
                                
                                ?>
                                <option value="<?= $data->KODE_MATKUL?>"><?= $data->NAMA_MATKUL." (".$data->NAMA_DOSEN.")"?></option>
                                <?php 
                                    }
                                ?>
                                
                            </select>
                                
                        </div>
                        <div class="form-group">
                            <label>Pilih Jenis Ujian</label>
                            
                            <select  name="id_jenis_ujian" class="form-control" >
                                <option value="">---Pilih Jenis Ujian---</option>
                                <?php 
                                    foreach ($data_jenis_ujian as $data) {

                                    
                                ?>
                                <option value="<?= $data->ID_JENIS?>" ><?= $data->NAMA_JENIS ?></option>
                                <?php 
                                    }
                                ?>
                            </select>
                                
                        </div>
                        
                        <div class="form-group">
                            <label>Pilih Tanggal Ujian</label>
                            
                            <input class="form-control tgl" type="text" name="tgl" placeholder="TANGGAL UJIAN"/> 
                                
                        </div>
                        <div class="form-group">
                            <label>Pilih Jam Ujian</label>
                            
                            <input class="form-control timepicker" type="text" name="jam" placeholder="JAM UJIAN"/> 
                                
                        </div>
                        <div class="form-group">
                            <label>Masukkan Keterangan</label>
                            <input class="form-control" type="text" name="keterangan" placeholder="KETERANGAN" />
                        </div>
                        
                        <div class="form-group footer">
                            <button class="btn btn-success" type="submit"><li class="fa fa-check"></li> Simpan</button>
                        </div>
                        </form>
                       
                    </div><!-- /.box-header -->

                  

                </div><!-- /.box -->
            </div>
            <div class=" col-md-8">
               
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Ujian</h3>
                    </div><!-- /.box-header -->
                    
                    <div class="box-body table-responsive">
                        <table id="test" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="col-xs-2">Jenis Ujian</th>
                                    <th class="col-xs-2">Nama Ujian</th>
                                    
                                    <th class="col-xs-2">Tanggal/Jam</th>
                                    
                                    <th class="col-xs-2">Keterangan</th>
                                    <th class="col-xs-2">Nilai</th>
                                    
                                    <th class="col-xs-1">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($data_ujian as $data) {
                                        
                                    
//                                ?>
                                        <tr>
                                            <td class=""><?= $data->NAMA_JENIS ?></td>
                                            <td class=""><?= $data->NAMA_UJIAN ?></td>
                                            
                                            <td class=""><?= date_format(date_create($data->TGL), "d M Y")." ".$data->JAM ?></td>
                                            <td ><?= $data->KETERANGAN?></td>
                                            <td ><?= $data->NILAI ?></td>
                                            
                                            <td>
                                                
                                                <a href="#" data-toggle="modal" data-target="#editDosen" onclick="getData('<?= $data->ID_UJIAN ?>','<?= $data->KODE_MATKUL ?>','<?= $data->ID_JENIS ?>','<?= $data->TGL ?>','<?= $data->JAM ?>','<?= $data->KETERANGAN ?>','<?= $data->NILAI ?>')"><li class="fa fa-pencil-square-o" data-toggle="tooltip" title="Edit"  ></li></a>
                                                <a href="#" data-toggle="modal" data-target="#konfirmasi" onclick="getId('<?= $data->ID_UJIAN ?>')"><li class="fa fa-trash-o" data-toggle="tooltip" title="Hapus"  ></li></a>
                                            </td>
                                        </tr>
                               <?php 
                                    }
                               ?>
                            </tbody>
    <!--                        <tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                </tr>
                            </tfoot>-->
                        </table>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box -->
                
            </div>
            
            <div class="modal fade" id="konfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-blue-gradient">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                        </div>
                        
                        <div class="modal-body bg-gray">
                            <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_ujian','deleteUjian',array())) ?>
                            <div class="form-group">
                                <label>YAKIN MENGHAPUS ? </label>
                                <input class="hidden" name="id_ujian" id="id_ujian"/>
                                <br>
                                <button type="submit" class="btn btn-success" ><li class="fa fa-check"></li> Hapus</button>

                            </div>

                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="editDosen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-blue-gradient">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Edit Data Ujian</h4>
                        </div>
                        <div class="modal-body bg-gray">
                            <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_ujian','updateUjian',array())) ?>
                            <div hidden="">
                                <input name="id_ujian" id="id" />
                                <input name="kode_matkul" id="matkul" />
                            </div>
                            
                            <div class="form-group">
                                <label>Jenis Ujian</label>

                                <select  name="id_jenis_ujian" class="form-control" id="jenis_ujian">
                                    <option value="">---Pilih Jenis Ujian---</option>
                                    <?php 
                                        foreach ($data_jenis_ujian as $data) {


                                    ?>
                                    <option value="<?= $data->ID_JENIS?>" ><?= $data->NAMA_JENIS ?></option>
                                    <?php 
                                        }
                                    ?>
                                </select>

                            </div>

                            <div class="form-group">
                                <label>Tanggal Ujian</label>

                                <input class="form-control tgl" id="tgl" type="text" name="tgl" placeholder="TANGGAL UJIAN"/> 

                            </div>
                            <div class="form-group">
                                <label>Jam Ujian</label>

                                <input class="form-control timepicker" id="jam" type="text" name="jam" placeholder="JAM UJIAN"/> 

                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>
                                <input class="form-control" type="text" id="keterangan" name="keterangan" placeholder="KETERANGAN" />
                            </div>
                            <div class="form-group">
                                <label>Nilai</label>
                                <input class="form-control" type="text" id="nilai" name="nilai" placeholder="NILAI" />
                            </div>
                            <div class="form-group">

                                <button type="submit" class="btn btn-success" ><li class="fa fa-check"></li> Simpan</button>
                            </div>
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section><!-- /.content -->

    <!--History Jabatan-->


</aside><!-- /.right-side -->

</div>
