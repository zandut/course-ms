<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Perkuliahan
            <small>Mulai Kuliah</small>
            
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Perkuliahan</a></li>
            <li class="active">Mulai Kuliah</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php 
                    if ($success == TRUE)
                    {
                ?>
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>SUCCESS !!</b>
                        </div>
                <?php 
                    }
                    else
                        if ($error == TRUE)
                        {
                           
                ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-warning"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>PERHATIAN !!</b><br><?= validation_errors()?>
                            </div>
                <?php 
                        }
                ?>
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Catatan Mata Kuliah</h3>
                    </div><!-- /.box-header -->
                    
                    <div class="box-body">
                        <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_mulai_kuliah','mulaiKuliah')) ?>
                        <div class="form-group">
                            

                            <textarea id="editor1" name="catatan" rows="10" cols="80">
                                <?php 
                                   echo $data_chapter->CATATAN;
                                ?>
                            </textarea>

                        </div>
                        <div hidden="">
                            <input name="id_chapter" value="<?= $data_chapter->ID_CHAPTER?>" />
                            <input name="kode_matkul" value="<?= $data_chapter->KODE_MATKUL?>" />
                        </div>
                        
                        <div class="footer">
                            <a href="<?= site_url($this->mza_secureurl->setSecureUrl_encode('ctrl_mulai_kuliah','index'))?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
                            <button class="btn btn-success" type="submit"><li class="fa fa-check"> Simpan</li></button>
                        </div>
                        </form>
                        

                    </div>
                </div><!-- /.box -->
            </div>
            <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-blue-gradient">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Tambah Chapter</h4>
                        </div>

                        <div class="modal-body bg-gray">
                            <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_mulai_kuliah','insertChapter')) ?>
                            <div class="form-group">
                                <label>Pilih Mata Kuliah</label>
                                <select class="form-control" name="kode_matkul" >
                                    <option value="">---Pilih Mata Kuliah---</option>
                                    <?php 
                                        foreach ($data_kuliah as $data) {

                                    ?>
                                    <option value="<?= $data->KODE_MATKUL?>" ><?= $data->NAMA_MATKUL." (Bp. ".$data->NAMA_DOSEN.")"?></option>
                                    <?php
                                        }   
                                    ?>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>Nama Chapter</label>
                                <input class="form-control" name="nama_chapter" placeholder="NAMA CHAPTER"/>
                                
                            </div>
                            
                            
                            <div class="footer ">
                                <button type="submit" class="btn btn-success " ><li class="fa fa-check"></li> Simpan</button>
                                <button type="reset" class="btn btn-warning"><li class="fa fa-refresh"></li>  Reset</button>
                            </div>

                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>  
            
        
    </section><!-- /.content -->

    <!--History Jabatan-->


</aside><!-- /.right-side -->

</div>
