
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data
            <small>Data Jenis Ujian</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Data</a></li>
            <li class="active">Data Jenis Ujian</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class=" col-md-5">
                <?php 
                    if ($success == TRUE)
                    {
                ?>
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>SUCCESS !!</b>
                        </div>
                <?php 
                    }
                    else
                        if ($error == TRUE)
                        {
                           
                ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-warning"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>PERHATIAN !!</b><br><?= validation_errors()?>
                            </div>
                <?php 
                        }
                ?>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Tambah Jenis Ujian</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body ">

                        <?= form_open($this->mza_secureurl->setSecureurl_encode('ctrl_jenis_ujian','insertJenisUjian',array())) ?>
                        <div class="form-group">
                            <label>Masukkan Jenis Ujian</label>
                            
                            <input class="form-control" type="text" name="jenis" placeholder="eg. UTS"/> 
                                
                        </div>
                        <div class="form-group footer">
                            <button class="btn btn-success" type="submit"><li class="fa fa-check"></li> Simpan</button>
                        </div>
                        </form>
                       
                    </div><!-- /.box-header -->

                  

                </div><!-- /.box -->
            </div>
            <div class=" col-md-7">
               
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Jenis Ujian</h3>
                    </div><!-- /.box-header -->
                    <script >
                        function getId(id)
                        {
                            document.getElementById("id_jenis").value = id;
                        }
                        
                        function getJenisUjian(id)
                        {
                            if (window.XMLHttpRequest) {
                                // code for IE7+, Firefox, Chrome, Opera, Safari
                                xmlhttp = new XMLHttpRequest();
                            } else { // code for IE6, IE5
                                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                            }
                            xmlhttp.onreadystatechange = function() {
                                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                    document.getElementById("jenis").innerHTML = xmlhttp.responseText;
                                }
                            }
                            var base = window.location.origin;
            //                alert(base + "/aplikasihrd/index.php/ctrl_bagian/getBagian?id=" + str);

                            xmlhttp.open("GET", base + "/course-ms/index.php/ctrl_jenis_ujian/getJenisUjian?id=" + id, true);
                            xmlhttp.send();
                        }
                    
                    </script>
                    <div class="box-body table-responsive">
                        <table id="test" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="col-xs-1">Jenis Ujian</th>
                                    <th class="col-xs-1">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($data_jenis_ujian as $data) {
                                        
                                    
//                                ?>
                                        <tr>
                                            <td class=""><?= $data->NAMA_JENIS ?></td>
                                            <td>
                                                <a href="#" data-toggle="modal" data-target="#editJenisUjian" onclick="getJenisUjian('<?= $data->ID_JENIS ?>')"><li class="fa fa-pencil-square-o" data-toggle="tooltip" title="Edit"  ></li></a>
                                                <a href="#" data-toggle="modal" data-target="#konfirmasi" onclick="getId('<?= $data->ID_JENIS ?>')"><li class="fa fa-trash-o" data-toggle="tooltip" title="Hapus"  ></li></a>
                                            </td>
                                        </tr>
                               <?php 
                                    }
                               ?>
                            </tbody>
    <!--                        <tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                </tr>
                            </tfoot>-->
                        </table>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box -->
                
            </div>
            
            <div class="modal fade" id="konfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-blue-gradient">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                        </div>
                        
                        <div class="modal-body bg-gray">
                            <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_jenis_ujian','deleteJenisUjian',array())) ?>
                            <div class="form-group">
                                <label>YAKIN MENGHAPUS ? </label>
                                <input class="hidden" name="id_jenis" id="id_jenis"/>
                                <br>
                                <button type="submit" class="btn btn-success" ><li class="fa fa-check"></li> Hapus</button>

                            </div>

                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="editJenisUjian" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-blue-gradient">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Edit Jenis Ujian</h4>
                        </div>
                        <div class="modal-body bg-gray">
                            <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_jenis_ujian','updateJenisUjian',array())) ?>
                            <div class="form-group">
                                <label>Jenis Ujian</label>
                                
                                <div id="jenis"></div>
                                <br>
                                <button type="submit" class="btn btn-success" ><li class="fa fa-check"></li> Simpan</button>

                            </div>

                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section><!-- /.content -->

    <!--History Jabatan-->


</aside><!-- /.right-side -->

</div>
