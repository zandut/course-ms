
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Perkuliahan
            <small>Tugas Kuliah</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Perkuliahan</a></li>
            <li class="active">Tugas Kuliah</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class=" col-md-4">
                <?php 
                    if ($success == TRUE)
                    {
                ?>
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>SUCCESS !!</b>
                        </div>
                <?php 
                    }
                    else
                        if ($error == TRUE)
                        {
                           
                ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-warning"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>PERHATIAN !!</b><br><?= validation_errors()?>
                            </div>
                <?php 
                        }
                ?>
                <script >
                    function getId(id)
                    {
                        document.getElementById("id_tugas").value = id;
                    }
                    
                    function getData(id, judul, tgl, jam, nilai, status)
                    {
                        document.getElementById("judul_tugas").value = judul;
                        document.getElementById("tgl_kumpul").value = tgl;
                        document.getElementById("jam_kumpul").value = jam;
                        document.getElementById("nilai").value = nilai;
                        document.getElementById("status").value = status;
                        document.getElementById("id").value = id;
                    }

                </script>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Tambah Tugas</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body ">

                        <?= form_open($this->mza_secureurl->setSecureurl_encode('ctrl_tugas','insertTugas',array())) ?>
                        <div class="form-group">
                            <label>Pilih Mata Kuliah</label>
                            <select class="form-control" name="kode_matkul" onchange="showChapter(this.value)" >
                                <option value="" >---Pilih Matkul---</option>
                                <?php 
                                    foreach ($data_matkul as $data) {
                                                
                                
                                ?>
                                <option value="<?= $data->KODE_MATKUL?>"><?= $data->NAMA_MATKUL." (".$data->NAMA_DOSEN.")"?></option>
                                <?php 
                                    }
                                ?>
                                
                            </select>
                                
                        </div>
                        <div class="form-group">
                            <label>Pilih Chapter</label>
                            
                            <select id="chapter" name="id_chapter" class="form-control" >
                                <option value="">---Pilih Chapter---</option>
                            </select>
                                
                        </div>
                        <div class="form-group">
                            <label>Masukkan Judul Tugas</label>
                            
                            <input class="form-control" type="text" name="judul_tugas" placeholder="JUDUL TUGAS"/> 
                                
                        </div>
                        <div class="form-group">
                            <label>Pilih Tanggal Pengumpulan</label>
                            
                            <input class="form-control tgl" type="text" name="tgl" placeholder="TANGGAL PENGUMPULAN"/> 
                                
                        </div>
                        <div class="form-group">
                            <label>Pilih Jam Pengumpulan</label>
                            
                            <input class="form-control timepicker" type="text" name="jam" placeholder="JAM PENGUMPULAN"/> 
                                
                        </div>
                        
                        <div class="form-group footer">
                            <button class="btn btn-success" type="submit"><li class="fa fa-check"></li> Simpan</button>
                        </div>
                        </form>
                       
                    </div><!-- /.box-header -->

                  

                </div><!-- /.box -->
            </div>
            <div class=" col-md-8">
               
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Tugas</h3>
                    </div><!-- /.box-header -->
                    
                    <div class="box-body table-responsive">
                        <table id="test" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="col-xs-2">Nama Matkul</th>
                                    <th class="col-xs-2">Chapter</th>
                                    <th class="col-xs-2">Judul Tugas</th>
                                    <th class="col-xs-2">Pengumpulan</th>
                                    <th class="col-xs-1">Nilai</th>
                                    <th class="col-xs-1">Status</th>
                                    <th class="col-xs-1">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($data_tugas as $data) {
                                        
                                    
//                                ?>
                                        <tr>
                                            <td class=""><?= $data->NAMA_MATKUL ?></td>
                                            <td class=""><?= $data->NAMA_CHAPTER ?></td>
                                            <td class=""><?= $data->JUDUL_TUGAS ?></td>
                                            <td class=""><?= date_format(date_create($data->TGL_KUMPUL), "d/m/Y")." ".$data->JAM_KUMPUL ?></td>
                                            <td ><?= $data->NILAI?></td>
                                            <?php 
                                                if ($data->STATUS == '1')
                                                {
                                                    
                                                
                                            ?>
                                            <td style="color: green"><label>SELESAI</label></td>
                                            <?php 
                                                }
                                                else
                                                {
                                            ?>
                                            <td style="color: red"><label>BELUM SELESAI</label></td>
                                            <?php 
                                                }
                                            ?>
                                            
                                            <td>
                                                <?php 
                                                    if ($data->STATUS != '1')
                                                    {
                                                ?>
                                                    <a href="#" ><li class="fa fa-mail-forward" data-toggle="tooltip" title="Kirim E-mail"></li></a>
                                                <?php 
                                                    }
                                                ?>
                                                <a href="#" data-toggle="modal" data-target="#editDosen" onclick="getData('<?= $data->ID_TUGAS ?>','<?= $data->JUDUL_TUGAS ?>','<?= $data->TGL_KUMPUL ?>','<?= $data->JAM_KUMPUL ?>','<?= $data->NILAI ?>','<?= $data->STATUS ?>')"><li class="fa fa-pencil-square-o" data-toggle="tooltip" title="Edit"  ></li></a>
                                                <a href="#" data-toggle="modal" data-target="#konfirmasi" onclick="getId('<?= $data->ID_TUGAS ?>')"><li class="fa fa-trash-o" data-toggle="tooltip" title="Hapus"  ></li></a>
                                            </td>
                                        </tr>
                               <?php 
                                    }
                               ?>
                            </tbody>
    <!--                        <tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                </tr>
                            </tfoot>-->
                        </table>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box -->
                
            </div>
            
            <div class="modal fade" id="konfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-blue-gradient">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                        </div>
                        
                        <div class="modal-body bg-gray">
                            <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_tugas','deleteTugas',array())) ?>
                            <div class="form-group">
                                <label>YAKIN MENGHAPUS ? </label>
                                <input class="hidden" name="id_tugas" id="id_tugas"/>
                                <br>
                                <button type="submit" class="btn btn-success" ><li class="fa fa-check"></li> Hapus</button>

                            </div>

                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="editDosen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-blue-gradient">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Edit Data Dosen</h4>
                        </div>
                        <div class="modal-body bg-gray">
                            <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_tugas','updateTugas',array())) ?>
                            <div hidden="">
                                <input name="id_tugas" id="id" />
                            </div>
                            <div class="form-group">
                                <label>Judul Tugas</label>

                                <input class="form-control" type="text" id="judul_tugas" name="judul_tugas" placeholder="JUDUL TUGAS"/> 

                            </div>
                            <div class="form-group">
                                <label>Tanggal Pengumpulan</label>

                                <input class="form-control tgl" type="text" id="tgl_kumpul" name="tgl" placeholder="TANGGAL PENGUMPULAN"/> 

                            </div>
                            <div class="form-group">
                                <label>Jam Pengumpulan</label>

                                <input class="form-control timepicker" type="text" id="jam_kumpul" name="jam" placeholder="TANGGAL PENGUMPULAN"/> 

                            </div>
                            <div class="form-group">
                                <label>Nilai Tugas</label>

                                <input class="form-control" type="text" id="nilai" name="nilai" placeholder="NILAI"/> 

                            </div>
                            <div class="form-group">
                                <label>STATUS</label>
                                <select id="status" name="status" class="form-control">
                                    <option value="0">Belum Selesai</option>
                                    <option value="1">Selesai</option>
                                </select>
                                

                            </div>
                            <div class="form-group">
                                
                                <button type="submit" class="btn btn-success" ><li class="fa fa-check"></li> Simpan</button>
                            </div>
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section><!-- /.content -->

    <!--History Jabatan-->


</aside><!-- /.right-side -->

</div>
