
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data
            <small>Data Tahun Akademik</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Data</a></li>
            <li class="active">Data Tahun Akademik</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class=" col-md-5">
                <?php 
                    if ($success == TRUE)
                    {
                ?>
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>SUCCESS !!</b>
                        </div>
                <?php 
                    }
                    else
                        if ($error == TRUE)
                        {
                           
                ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-warning"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>PERHATIAN !!</b><br><?= validation_errors()?>
                            </div>
                <?php 
                        }
                ?>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Tambah Tahun Akademik</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body ">

                        <?= form_open($this->mza_secureurl->setSecureurl_encode('ctrl_tahun','insertTahun',array())) ?>
                        <div class="form-group">
                            <label>Masukkan Tahun</label>
                            
                            <input class="form-control" type="text" name="tahun" placeholder="eg. <?= date('Y', time())?>/<?= date('Y', time()) + 1?>"/> 
                                
                        </div>
                        <div class="form-group footer">
                            <button class="btn btn-success" type="submit"><li class="fa fa-check"></li> Simpan</button>
                        </div>
                        </form>
                       
                    </div><!-- /.box-header -->

                  

                </div><!-- /.box -->
            </div>
            <div class=" col-md-7">
               
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Tahun Akademik</h3>
                    </div><!-- /.box-header -->
                    <script >
                        function getId(id)
                        {
                            document.getElementById("tahun").value = id;
                        }
                    
                    </script>
                    <div class="box-body table-responsive">
                        <table id="test" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="col-xs-1">Tahun Akademik</th>
                                    <th class="col-xs-1">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($data_tahun as $data) {
                                        
                                    
//                                ?>
                                        <tr>
                                            <td class=""><?= $data->TAHUN?></td>
                                            <td>
                                                
                                                <a href="#" data-toggle="modal" data-target="#konfirmasi" onclick="getId('<?= $data->TAHUN ?>')"><li class="fa fa-trash-o" data-toggle="tooltip" title="Hapus"  ></li></a>
                                            </td>
                                        </tr>
                               <?php 
                                    }
                               ?>
                            </tbody>
    <!--                        <tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                </tr>
                            </tfoot>-->
                        </table>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box -->
                
            </div>
            <div class=" col-md-5">
               
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Set Tahun Akademik</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <?php 
                            if ($data_tahun_semester == NULL)
                            {
                                $text = "TIDAK ADA";
                                $color = "red";
                            }
                            else
                            {
                                $text = $data_tahun_semester->TAHUN." - ".$data_tahun_semester->NAMA_SEMESTER;
                                $color = "green";
                            }
                        
                        ?>
                        Tahun Akademik Aktif : <label style="color: <?= $color?>"><?= $text ?></label>
                        
                        <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_tahun','setTahunAkademik',array())) ?>
                        <div class="form-group">
                            <label>Pilih Tahun Akademik</label>
                            <select name="tahun" class="form-control">
                                <option>Pilih Tahun</option>
                                <?php 
                                    foreach ($data_tahun as $data) {

                                ?>
                                    <option value="<?= $data->TAHUN?>"><?= $data->TAHUN?></option>
                                <?php 
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Pilih Semester</label>
                            <select name="semester" class="form-control">
                                <option>Pilih Semester</option>
                                <?php 
                                    foreach ($data_semester as $data) {

                                ?>
                                    <option value="<?= $data->ID_SEMESTER?>"><?= $data->NAMA_SEMESTER?></option>
                                <?php 
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="footer">
                            <button type="submit" class="btn btn-success"><li class="fa fa-check"></li> Simpan</button>
                        </div>
                        
                        </form>
                    </div>
                    
                    
                </div><!-- /.box -->
                
            </div>
            <div class="modal fade" id="konfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-blue-gradient">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                        </div>
                        <div class="modal-body bg-gray">
                            <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_tahun','deleteTahun',array())) ?>
                            <div class="form-group">
                                <label>YAKIN MENGHAPUS ? </label>
                                <input class="hidden" name="tahun" id="tahun"/>
                                <br>
                                <button type="submit" class="btn btn-success" ><li class="fa fa-check"></li> Hapus</button>

                            </div>

                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section><!-- /.content -->

    <!--History Jabatan-->


</aside><!-- /.right-side -->

</div>
