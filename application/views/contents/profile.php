<aside class="right-side">                
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Detail Profile
            <small>Edit Profile</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="">Detail Profile</li>
            
            <li class="active">Edit Profile</li>
        </ol>
    </section>

    <section class="content">
        <div class="row ">
            <div class="col-md-12">
                <?php 
                if ($success == TRUE)
                {
                ?>
                    <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b>SUCCESS !!</b>
                    </div>
                <?php 
                }
                else
                    if ($error == TRUE)
                    {
                ?>
                    <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-warning"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b>PERHATIAN !!</b><br><?= validation_errors()?>
                    </div>
                <?php 
                
                    }
                    else
                        if ($salah == TRUE)
                        {
                ?>
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-warning"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>PERHATIAN !!</b><br>Password yang lama tidak sama !!
                        </div>
                <?php 

                        }
                ?>
               
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Profile</h3>
                    </div>
                    <div class="box-body ">
                        
                            <!-- Custom Tabs -->
                            <div class="nav-tabs-custom ">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1" data-toggle="tab">Detail Profile</a></li>
                                    <li><a href="#tab_2" data-toggle="tab">Ubah Password</a></li>
                                    
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">
                                            
                                        <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_mahasiswa','updateProfile'))?>
                                            <div class="form-group">
                                                <label>USERNAME</label>
                                                <input class="form-control" type="text" name="username" value="<?= $nama?>" readonly/>
                                            </div>
                                            <div class="form-group">
                                                <label>NAMA</label>
                                                <input class="form-control" type="text" name="nama" value="<?= $nama_depan?>"/>
                                            </div>
                                            <div class="form-group">
                                                <label>ALAMAT</label>
                                                <input class="form-control" type="text" name="alamat" value="<?= $alamat?>"/>
                                            </div>
                                            <div class="form-group">
                                                <label>KODE KEAMANAN</label>
                                                <input class="form-control" type="password" name="kode" placeholder="UBAH KODE KEAMANAN" />
                                                <input class="form-control hidden" type="text" name="password" placeholder="UBAH KODE KEAMANAN" />
                                            </div>
                                            <div class="footer">
                                                <button class="btn btn-success ion-checkmark" type="submit"> Simpan</button>
                                            </div>

                                        </form>
                                       
                                    </div><!-- /.tab-pane -->
                                    <div class="tab-pane" id="tab_2">
                                        <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_mahasiswa','updatePassword'))?>
                                            <div class="form-group">
                                                <label>PASSWORD LAMA</label>
                                                <input class="form-control" type="password" name="password_lama" placeholder="PASSWORD LAMA" />
                                            </div>
                                            <div class="form-group">
                                                <label>PASSWORD BARU</label>
                                                <input class="form-control" type="password" name="password_baru" placeholder="PASSWORD BARU" />
                                            </div>
                                            <div class="form-group">
                                                <label>KONFIRMASI PASSWORD</label>
                                                <input class="form-control" type="password" name="konfirmasi" placeholder="KONFIRMASI PASSWORD" />
                                            </div>
                                            
                                            <div class="footer">
                                                <button class="btn btn-success ion-checkmark" type="submit"> Simpan</button>
                                            </div>

                                        </form>
                                    </div><!-- /.tab-pane -->
                                </div><!-- /.tab-content -->
                            </div><!-- nav-tabs-custom -->
                        
                    </div>  <!--

                    Modals
                    <div class="modal fade" id="insert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                </div>
                                <div class="modal-body bg-gray">
                                    <?= form_open($this->mza_secureurl->setSecureUrl_encode('ctrl_bagian','deletebagian',array($data_bagian->ID_BAGIAN))) ?>
                                    <div class="form-group">
                                        <label>YAKIN MENGHAPUS ? </label>
                                        <br>
                                        <button type="submit" class="btn btn-success " >Hapus</button>
                                        <button data-dismiss="modal" class="btn btn-warning " >Batal</button>
                                    </div>
                                    </form>
                                    

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> /.box -->

                <!--History Jabatan-->

                </div>
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->