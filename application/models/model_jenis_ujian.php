<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class model_jenis_ujian extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function getData() {
        $this->db->from('jenis_ujian');
        
        return $this->db->get()->result();
    }
    
    public function getDataById($id) {
        $this->db->from('jenis_ujian');
        $this->db->where('ID_JENIS', $id);
        
        return $this->db->get()->row();
    }
    
    public function insertJenisUjian($jenis) {
        $this->db->set('NAMA_JENIS', $jenis);
        
        return $this->db->insert('jenis_ujian');
    }
    
    public function updateJenisUjian($id, $jenis) {
        
        $this->db->set('NAMA_JENIS', $jenis);
        $this->db->where('ID_JENIS', $id);
        
        return $this->db->update('jenis_ujian');
        
    }
    
    public function deleteJenisUjian($id) {
        $this->db->where('ID_JENIS', $id);
        return $this->db->delete('jenis_ujian');
    }
    
    
}
    