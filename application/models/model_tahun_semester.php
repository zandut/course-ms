<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class model_tahun_semester extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function insertTahunSemester($tahun, $id_semester) {
        $this->db->set('TAHUN',  $tahun);
        $this->db->set('ID_SEMESTER', $id_semester);
        $this->db->set('IS_AKTIF','0');
        
        return $this->db->insert('tahun_semester');
    }
    
    public function deleteTahunSemester($id_tahun_semester) {
        $this->db->where('ID_TAHUN_SEMESTER', $id_tahun_semester);
        
        return $this->db->delete('tahun_semester');
    }
    
    public function updateTahunSemester($tahun, $id_semester, $is_aktif) {
        $this->db->set('IS_AKTIF', $is_aktif);
        
        $this->db->where('TAHUN', $tahun);
        $this->db->where('ID_SEMESTER', $id_semester);
        
        return $this->db->update('tahun_semester');
    }
    
    public function getDataByIsAktif($is_aktif) {
        $this->db->from('tahun_semester');
        $this->db->join('semester','semester.ID_SEMESTER = tahun_semester.ID_SEMESTER');
        
        $this->db->where('IS_AKTIF', $is_aktif);
        
        return $this->db->get()->row();
    }
    
    
}