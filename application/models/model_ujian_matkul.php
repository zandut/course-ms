<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class model_ujian_matkul extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function insertUjianMatkul($id_ambil, $id_ujian) {
        $this->db->set('ID_AMBIL', $id_ambil);
        $this->db->set('ID_UJIAN', $id_ujian);
        $this->db->set('NILAI', '0');
        
        return $this->db->insert('ujian_matkul');
        
    }
    
    public function updateUjianMatkul($nilai, $id_ambil, $id_ujian) {
       
        
        $this->db->set('NILAI', $nilai);
        $this->db->where('ID_AMBIL', $id_ambil);
        $this->db->where('ID_UJIAN', $id_ujian);
        
        return $this->db->update('ujian_matkul');
        
    }
    
    public function deleteUjianMatkul($id_ambil, $id_ujian) {
        $this->db->where('ID_AMBIL', $id_ambil);
        $this->db->where('ID_UJIAN', $id_ujian);
        
        return $this->db->delete('ujian_matkul');
    }
    
    public function getDataByNim($nim) {
        $this->db->from('ujian_matkul');
        $this->db->join('ujian','ujian.ID_UJIAN = ujian_matkul.ID_UJIAN');
        $this->db->join('ambil_matkul','ambil_matkul.ID_AMBIL = ujian_matkul.ID_AMBIL');
        $this->db->join('mata_kuliah','mata_kuliah.KODE_MATKUL = ambil_matkul.KODE_MATKUL');
        $this->db->join('jenis_ujian','jenis_ujian.ID_JENIS = ujian.ID_JENIS');
        
        $this->db->where('ambil_matkul.NIM', $nim);
        
        return $this->db->get()->result();
        
    }
    
    
}