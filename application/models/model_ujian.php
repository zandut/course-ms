<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class model_ujian extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function insertUjian($id_jenis, $nama_ujian, $tgl, $jam, $keterangan) {
        $this->db->set('ID_JENIS', $id_jenis);
        $this->db->set('NAMA_UJIAN', $nama_ujian);
        $this->db->set('TGL', date_format(date_create($tgl), "Y-m-d"));
        $this->db->set('JAM', $jam);
        $this->db->set('KETERANGAN', $keterangan);
        
        return $this->db->insert('ujian');
    }
    
    public function updateUjian($id_jenis, $nama_ujian, $tgl, $jam, $keterangan, $id_ujian) {
        $this->db->set('ID_JENIS', $id_jenis);
        $this->db->set('NAMA_UJIAN', $nama_ujian);
        $this->db->set('TGL', date_format(date_create($tgl), "Y-m-d"));
        $this->db->set('JAM', $jam);
        $this->db->set('KETERANGAN', $keterangan);
        
        $this->db->where('ID_UJIAN', $id_ujian);
        
        return $this->db->update('ujian');
    }
    
    public function deleteUjian($id_ujian) {
        $this->db->where('ID_UJIAN', $id_ujian);
        
        return $this->db->delete('ujian');
    }
    
    public function getLastData() {
        $query = $this->db->query('Select * from ujian order by id_ujian desc limit 1');
        
        return $query->row();
    }
    
    public function getDataById($id_ujian) {
        $this->db->from('ujian');
        $this->db->where('ID_UJIAN', $id_ujian);
        
        return $this->db->get()->row();
    }
}