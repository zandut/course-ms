<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class model_ambil_matkul extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function getJumlahMatkul($username) {
        $this->db->from('mata_kuliah');
        $this->db->join('ambil_matkul', 'ambil_matkul.KODE_MATKUL = mata_kuliah.KODE_MATKUL');
        $this->db->join('tahun_semester', 'tahun_semester.ID_TAHUN_SEMESTER = mata_kuliah.ID_TAHUN_SEMESTER');
        $this->db->where('IS_AKTIF', '1');
        $this->db->where('NIM', $username);
        
        return $this->db->count_all_results();
    }
    
    public function getMatkul($username) {
        $this->db->from('mata_kuliah');
        $this->db->join('ambil_matkul', 'ambil_matkul.KODE_MATKUL = mata_kuliah.KODE_MATKUL');
        $this->db->join('dosen','mata_kuliah.KODE_DOSEN=dosen.KODE_DOSEN');
        $this->db->join('tahun_semester', 'tahun_semester.ID_TAHUN_SEMESTER = mata_kuliah.ID_TAHUN_SEMESTER');
        $this->db->where('IS_AKTIF', '1');
        $this->db->where('NIM', $username);
        
        return $this->db->get()->result();
    }
    
    public function ambil_matkul($kode_matkul, $nim) {
        $this->db->set('KODE_MATKUL', $kode_matkul);
        $this->db->set('NIM', $nim);
        
        return $this->db->insert('ambil_matkul');
    }
    
    public function cabut_matkul($kode_matkul, $nim) {
        $this->db->where('KODE_MATKUL', $kode_matkul);
        $this->db->where('NIM', $nim);
        
        return $this->db->delete('ambil_matkul');
    }
    
    public function getMatkulByKode($username, $kode) {
        $this->db->from('mata_kuliah');
        $this->db->join('ambil_matkul', 'ambil_matkul.KODE_MATKUL = mata_kuliah.KODE_MATKUL');
        $this->db->join('dosen','mata_kuliah.KODE_DOSEN=dosen.KODE_DOSEN');
        $this->db->join('tahun_semester', 'tahun_semester.ID_TAHUN_SEMESTER = mata_kuliah.ID_TAHUN_SEMESTER');
        $this->db->where('IS_AKTIF', '1');
        $this->db->where('NIM', $username);
        $this->db->where('mata_kuliah.KODE_MATKUL', $kode);
        
        return $this->db->get()->row();
        
    }
}