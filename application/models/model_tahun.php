<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class model_tahun extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function insertTahun($tahun) {
        $this->db->set('TAHUN', $tahun);
        
        return $this->db->insert('tahun_akademik');
    }
    
    public function deleteTahun($tahun) {
        $this->db->where('TAHUN',$tahun);
        
        return $this->db->delete('tahun_akademik');
    }
    
    public function getData() {
        $this->db->from('tahun_akademik');
        
        return $this->db->get()->result();
    }
    
    public function getDataByTahun($tahun) {
        
        $this->db->from('tahun_akademik');
        $this->db->where('TAHUN',  $tahun);
        
        return $this->db->get()->row();
    }
    
    
}