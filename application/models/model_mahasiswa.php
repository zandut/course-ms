<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class model_mahasiswa extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function getData($nim) {
        $this->db->from('mahasiswa');
        $this->db->where('NIM', $nim);
        
        return $this->db->get()->row();
    }
    
    public function insertMahasiswa($nim, $nama, $alamat, $password) {
        $this->db->set('NIM', $nim);
        $this->db->set('NAMA', $nama);
        $this->db->set('ALAMAT', $alamat);
        $this->db->set('PASSWORD', md5($password));
        $this->db->set('KODE_AKTIFASI',  md5("1234"));
        
        return $this->db->insert('mahasiswa');
    }
    
    public function updateMahasiswa($nim, $nama, $alamat, $kode_aktifasi) {
        $this->db->set('NAMA',$nama);
        $this->db->set('ALAMAT',$alamat);
       
        if ($kode_aktifasi != '')
            $this->db->set('KODE_AKTIFASI', md5($kode_aktifasi));
        
        $this->db->where('NIM', $nim);
        
        return $this->db->update('mahasiswa');
    }
    
    public function updatePassword($nim, $password) {
        $this->db->set('PASSWORD',  md5($password));
        $this->db->where('NIM', $nim);
        
        return $this->db->update('mahasiswa');
        
    }
    
    
}