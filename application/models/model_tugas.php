<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class model_tugas extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function getJumlahTugasByStatus($status)
    {
        $this->db->from('tugas');
        $this->db->where('STATUS', $status);
        
        return $this->db->count_all_results();
    }
    
    public function insertTugas($id_chapter, $judul_tugas, $tgl, $jam) {
        $this->db->set('ID_CHAPTER', $id_chapter);
        $this->db->set('JUDUL_TUGAS', $judul_tugas);
        $this->db->set('TGL_KUMPUL', $tgl);
        $this->db->set('JAM_KUMPUL', $jam);
        $this->db->set('NILAI', 0);
        $this->db->set('STATUS', 0);
        
        return $this->db->insert('tugas');
    }
    
    public function updateTugas($id_chapter, $judul_tugas, $tgl, $jam, $nilai, $status, $id_tugas) {
        $this->db->set('ID_CHAPTER', $id_chapter);
        $this->db->set('JUDUL_TUGAS', $judul_tugas);
        $this->db->set('TGL_KUMPUL', $tgl);
        $this->db->set('JAM_KUMPUL', $jam);
        $this->db->set('NILAI', $nilai);
        $this->db->set('STATUS', $status);
        
        $this->db->where('ID_TUGAS', $id_tugas);
        
        return $this->db->update('tugas');
    }
    
    public function deleteTugas($id_tugas) {
        $this->db->where('ID_TUGAS', $id_tugas);
        
        return $this->db->delete('tugas');
    }
    
    public function getDataById($id_tugas) {
        $this->db->from('tugas');
        $this->db->where('ID_TUGAS', $id_tugas);
        
        return $this->db->get()->row();
    }
    
    public function getData() {
        $this->db->from('tugas');
        $this->db->join('chapter','tugas.ID_CHAPTER = chapter.ID_CHAPTER');
        $this->db->join('mata_kuliah','chapter.KODE_MATKUL = mata_kuliah.KODE_MATKUL');
        $this->db->join('tahun_semester','tahun_semester.ID_TAHUN_SEMESTER = mata_kuliah.ID_TAHUN_SEMESTER');
        $this->db->where('IS_AKTIF', '1');
        
        return $this->db->get()->result();
    }
    
    public function getTugasTerdekat($NIM) {
        $query = $this->db->query("SELECT * FROM tugas JOIN chapter USING (ID_CHAPTER) JOIN ambil_matkul "
                . "USING (KODE_MATKUL) JOIN mata_kuliah USING (KODE_MATKUL) "
                . "WHERE ((TGL_KUMPUL > current_date()) or (TGL_KUMPUL = current_date() "
                . "and JAM_KUMPUL >= current_time())) and NIM ='".$NIM."' and STATUS='0'"
                . "order by TGL_KUMPUL, JAM_KUMPUL asc limit 1");
        return $query->row();
    }
    
    
}