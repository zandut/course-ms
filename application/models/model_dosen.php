<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class model_dosen extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function insertDosen($kode, $nama, $email, $no) {
        $this->db->set('KODE_DOSEN', $kode);
        $this->db->set('NAMA_DOSEN', $nama);
        $this->db->set('EMAIL', $email);
        $this->db->set('NO_HP', $no);
        
        return $this->db->insert('dosen');
    }
    
    public function updateDosen($kode, $nama, $email, $no) {
        $this->db->set('NAMA_DOSEN', $nama);
        $this->db->set('EMAIL', $email);
        $this->db->set('NO_HP', $no);   
        
        $this->db->where('KODE_DOSEN', $kode);
        
        return $this->db->update('dosen');
    }
    
    public function deleteDosen($kode) {
        $this->db->where('KODE_DOSEN', $kode);
        
        return $this->db->delete('dosen');
    }
    
    public function getData() {
        $this->db->from('dosen');
        
        return $this->db->get()->result();
    }
    
    public function getDataByKode($kode) {
        $this->db->from('dosen');
        
        $this->db->where('KODE_DOSEN',$kode);
        
        return $this->db->get()->row();
    }
}