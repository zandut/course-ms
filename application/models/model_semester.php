<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class model_semester extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function getData() {
        $this->db->from('semester');
        
        return $this->db->get()->result();
    }
}