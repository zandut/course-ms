<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class model_kuis extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function insertKuis($id_chapter, $nama_kuis, $tgl, $jam, $keterangan, $nilai) {
        $this->db->set('ID_CHAPTER', $id_chapter);
        $this->db->set('NAMA_KUIS', $nama_kuis);
        $this->db->set('TGL', date_format(date_create($tgl), "Y-m-d"));
        $this->db->set('JAM', $jam);
        $this->db->set('KETERANGAN', $keterangan);
        $this->db->set('NILAI', $nilai);
        
        return $this->db->insert('kuis');
    }
    
    public function updateKuis($id_chapter, $nama_kuis, $tgl, $jam, $keterangan, $nilai, $id_kuis) {
        $this->db->set('ID_CHAPTER', $id_chapter);
        $this->db->set('NAMA_KUIS', $nama_kuis);
        $this->db->set('TGL', date_format(date_create($tgl), "Y-m-d"));
        $this->db->set('JAM', $jam);
        $this->db->set('KETERANGAN', $keterangan);
        $this->db->set('NILAI', $nilai);
        
        $this->db->where('ID_KUIS', $id_kuis);
        
        return $this->db->update('kuis');
    }
    
    public function deleteKuis($id_kuis) {
        $this->db->where('ID_KUIS', $id_kuis);
        
        return $this->db->delete('kuis');
    }
    
    public function getData() {
        $this->db->from('kuis');
        $this->db->join('chapter','kuis.ID_CHAPTER = chapter.ID_CHAPTER');
        $this->db->join('mata_kuliah','chapter.KODE_MATKUL = mata_kuliah.KODE_MATKUL');
        $this->db->join('tahun_semester','tahun_semester.ID_TAHUN_SEMESTER = mata_kuliah.ID_TAHUN_SEMESTER');
        $this->db->where('IS_AKTIF', '1');
        return $this->db->get()->result();
        
    }
    
    public function getDataById($id_kuis) {
        $this->db->from('kuis');
        $this->db->join('chapter','kuis.ID_CHAPTER = chapter.ID_CHAPTER');
        $this->db->where('ID_KUIS', $id_kuis);
        
        return $this->db->get()->row();
        
    }
    
    public function getKuisTerdekat($NIM) {
        $query = $this->db->query("SELECT * FROM kuis JOIN chapter USING (ID_CHAPTER) "
                . "JOIN ambil_matkul USING (KODE_MATKUL) JOIN mata_kuliah USING (KODE_MATKUL) "
                . "WHERE ((TGL > current_date()) or (TGL = current_date() and JAM >= current_time())) "
                . "and NIM='".$NIM."' order by TGL, JAM asc limit 1 ");
        return $query->row();
    }
    
    
}