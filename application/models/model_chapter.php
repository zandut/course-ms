<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class model_chapter extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function insertChapter($kode_matkul, $nama_chapter, $catatan) {
        $this->db->set('KODE_MATKUL',$kode_matkul);
        $this->db->set('NAMA_CHAPTER',$nama_chapter);
        $this->db->set('CATATAN',$catatan);
        
        return $this->db->insert('chapter');
    }
    
    public function updateChapter($kode_matkul, $nama_chapter, $catatan, $id_chapter) {
        $this->db->set('KODE_MATKUL',$kode_matkul);
        $this->db->set('NAMA_CHAPTER',$nama_chapter);
        $this->db->set('CATATAN',$catatan);
        
        $this->db->where('ID_CHAPTER',$id_chapter);
        
        return $this->db->update('chapter');
    }
    
    public function deleteChapter($id_chapter) {
        $this->db->where('ID_CHAPTER',$id_chapter);
        
        return $this->db->delete('chapter');
    }
    
    public function getDataById($id_chapter) {
        $this->db->from('chapter');
        $this->db->join('mata_kuliah','mata_kuliah.KODE_MATKUL = chapter.KODE_MATKUL'); 
        $this->db->where('ID_CHAPTER', $id_chapter);
        
        return $this->db->get()->row();
    }
    
    public function getData() {
        $this->db->from('chapter');
        
        return $this->db->get()->result();
    }
    
    public function getDataByKodeMatkul($kode) {
        $this->db->from('chapter');
        $this->db->join('mata_kuliah','mata_kuliah.KODE_MATKUL = chapter.KODE_MATKUL');
        $this->db->where('chapter.KODE_MATKUL', $kode);
        
        return $this->db->get()->result();
    }
    
    public function getJumlahChapter($kode_matkul) {
        $this->db->from('chapter');
        $this->db->join('mata_kuliah','mata_kuliah.KODE_MATKUL = chapter.KODE_MATKUL');
        $this->db->where('chapter.KODE_MATKUL', $kode_matkul);
        
        return $this->db->count_all_results();
        
    }
}