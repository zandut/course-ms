<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class model_mata_kuliah extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    
    
    public function insertMatkul($kode_matkul, $kode_dosen, $id_tahun, $nama_matkul) {
        $this->db->set('KODE_MATKUL', $kode_matkul);
        $this->db->set('KODE_DOSEN', $kode_dosen);
        $this->db->set('ID_TAHUN_SEMESTER', $id_tahun);
        $this->db->set('NAMA_MATKUL', $nama_matkul);
        
        return $this->db->insert('mata_kuliah');
    }
    
    public function updateMatkul($kode_matkul, $kode_dosen, $nama_matkul) {
        $this->db->where('KODE_MATKUL', $kode_matkul);
        $this->db->set('KODE_DOSEN', $kode_dosen);
        
        $this->db->set('NAMA_MATKUL', $nama_matkul);
        
        return $this->db->update('mata_kuliah');
    }
    
    public function deleteMatkul($kode_matkul) {
        $this->db->where('KODE_MATKUL', $kode_matkul);
        
        return $this->db->delete('mata_kuliah');
    }
    
    
    
    
    
}